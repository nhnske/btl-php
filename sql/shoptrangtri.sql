-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 07:16 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoptrangtri`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `customer_id` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `create_at`, `customer_id`) VALUES
(1, 'admin', '25d55ad283aa400af464c76d713c07ad', '2019-11-04 15:59:19', ''),
(2, 'SoMot', 'e99a18c428cb38d5f260853678922e03', '2019-11-05 18:30:10', '1as8e1'),
(4, 'sohai', 'e99a18c428cb38d5f260853678922e03', '2019-11-05 22:10:50', '324775');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` varchar(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(50) DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `created_at`) VALUES
('19c5', 'Trang trí tủ kệ', 1, '2019-11-02 15:54:59'),
('2ec0', 'Bình hoa', 1, '2019-11-02 15:54:59'),
('8d9c', 'Đồng hồ', 1, '2019-11-02 15:54:59'),
('cf08', 'Tranh treo tường', 1, '2019-11-02 15:54:59');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` varchar(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `phone`, `email`) VALUES
('1as8e1', 'So Mot', 'Hanoi', '091111111', 'nhnske@gmail.com'),
('324775', 'a', 'a', '0982120398', 'nhnske@gmail.com'),
('922215', 'a', 'a', '0982120398', 'nhnske@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` varchar(6) NOT NULL,
  `customer_id` varchar(6) NOT NULL,
  `note` longtext DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `note`, `created_at`, `status`) VALUES
('243725', '1as8e1', 'Tránh ẩm ướt nhé!', '2019-11-05 23:16:15', NULL),
('316235', '1as8e1', 'Cẩn thận.', '2019-11-05 23:22:00', NULL),
('410025', '1as8e1', '', '2019-11-06 00:16:30', NULL),
('6d5h4d', '1as8e1', 'Đừng làm vỡ hàng nhé!', '2019-11-05 22:39:23', NULL),
('743765', '1as8e1', 'Cẩn thận nhé.', '2019-11-05 23:23:57', NULL),
('799105', '1as8e1', 'Cẩn thận.', '2019-11-05 23:20:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE `orders_products` (
  `order_id` varchar(6) NOT NULL,
  `product_id` varchar(6) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders_products`
--

INSERT INTO `orders_products` (`order_id`, `product_id`, `price`, `quantity`) VALUES
('243725', '01342b', 765000, 1),
('316235', '01342b', 765000, 1),
('410025', '117cc0', 265000, 2),
('6d5h4d', '0e14e1', 115000, 1),
('6d5h4d', '153569', 153569, 1),
('743765', '01342b', 765000, 1),
('799105', '01342b', 765000, 1);

--
-- Triggers `orders_products`
--
DELIMITER $$
CREATE TRIGGER `update_product` AFTER INSERT ON `orders_products` FOR EACH ROW UPDATE products
SET stock = stock - NEW.quantity
WHERE id = NEW.product_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` varchar(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `image` text DEFAULT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `status` int(50) DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `image`, `price`, `stock`, `status`, `created_at`) VALUES
('01342b', 'Đồng hồ sách điện thoại vintage đỏ', '8d9c', '2268434019c54fba989e311dd1a96181-300x300.jpg', 765000, 11, 1, '2019-11-05 16:34:34'),
('02cb5e', 'Hoa cúc cánh bướm màu trắng', '2ec0', '20c8bb98b752439694699626ee4b8b67-300x300.jpg', 169000, 6, 1, '2019-11-05 16:34:34'),
('04fd77', 'Ngọn hải đăng đồ trang trí biển phong cách địa tru', '19c5', 'mo-hinh-may-may-de-ban-gia-co-phong-cach-vintage-do-trang-tri-phong-cach-vintage-300x300.jpg', 70000, 17, 1, '2019-11-05 16:34:34'),
('081221', 'Đồng hồ treo tường hiện đại số la mã hình tròn', '8d9c', 'd464f17078854dc38612518f56d3af03-300x300.jpg', 215000, 17, 1, '2019-11-05 16:34:34'),
('0868c1', 'Đồng hồ để bàn báo thức thỏ màu hồng', '8d9c', '8af2517b98d54ae8b3b4898ebf388d56-300x300.jpg', 305000, 24, 1, '2019-11-05 16:34:34'),
('091f28', 'Đồng hồ hiện đại số cách điệu viền bạc 02', '8d9c', 'dong-ho-dau-huou-mat-xam-300x300.jpg', 695000, 15, 1, '2019-11-05 16:34:34'),
('09e6d4', 'Cối xoay gió vintage cao 02', '19c5', 'c46d846329884cd8b1f975768f9ac33d-300x300.jpg', 295000, 32, 1, '2019-11-05 16:34:34'),
('0c18c2', 'Đồng hồ vintage 28 Ngôi nhà và hoa', '8d9c', 'xe-hoi-dong-ho-vintage-do-trang-tri-gia-co-phong-cach-vintage-300x300.jpg', 265000, 10, 1, '2019-11-05 16:34:34'),
('0e14e1', 'Khung ảnh nhựa hình thoi lớn màu đen', '19c5', '984b186321b64cadb910b8b553964333-300x300.jpg', 115000, 20, 1, '2019-11-05 16:34:34'),
('0f5358', 'Cô dâu chú rể cao cấp', '19c5', 'd4a744f9b1b64fa99836945ffb6bc74f-300x300.jpg', 245000, 15, 1, '2019-11-05 16:34:34'),
('0f8921', 'Đồng hồ cát đen vuông 01', '8d9c', '1a28e9ac701e4cdaa9687507c0ec84ab-300x300.jpg', 215000, 18, 1, '2019-11-05 16:34:34'),
('117cc0', 'Đồng hồ hiện đại viền bạc 01', '8d9c', 'dong-ho-treo-tuong-hien-dai-trang-04-1-300x300.jpg', 265000, 25, 1, '2019-11-05 16:34:34'),
('120d37', 'Đồng hồ cành hoa quả lắc màu vàng', '8d9c', '27763aba764e47938f358c375724821c-300x300.jpg', 759000, 18, 1, '2019-11-05 16:34:34'),
('12632f', 'Tranh giấy phong cách vintage cảnh tháp effiel', 'cf08', '5939067d2d6b4afeb9ab594ed9b15c0e-300x300.jpg', 335000, 32, 1, '2019-11-05 16:34:35'),
('137aab', 'Tranh giấy phong cách vintage mèo have a nice day', 'cf08', 'f0d2f0ddcf88465c97195fd1497991c4-300x300.jpg', 850000, 14, 1, '2019-11-05 16:34:35'),
('153569', 'Cây lá cọ rùa lớn cao 70cm', '19c5', '031cde32e1fd4acda3dd0217f178cd2f-300x300.jpg', 545000, 25, 1, '2019-11-05 16:34:34'),
('15637f', 'Bộ 2 Tượng tay phật đôi trang trí', '19c5', 'dfd75ad61765492a90fdaa625637f031-300x300.jpg', 239000, 8, 1, '2019-11-05 16:34:34'),
('16db7b', 'Gương 1 chân màu vàng', '19c5', '54ae2025b61e4d0cb57ca5301a014bff-300x300.jpg', 255000, 20, 1, '2019-11-05 16:34:34'),
('172fbc', 'Hộp khăn giấy cờ Anh', '19c5', 'coi-xoay-gio-phong-cach-vintage-2-300x300.png', 289000, 13, 1, '2019-11-05 16:34:34'),
('18e492', 'Chậu cây xi măng nhắm mắt cười', '2ec0', 'bf73c11cbb7841f7b13045cb5bfa70a4-300x300.jpg', 235000, 19, 1, '2019-11-05 16:34:34'),
('193c86', 'Đồng hồ cát nhỏ', '8d9c', 'a7e6b09a3ccf4c2d85f54e605ad735c6-300x300.jpg', 206000, 30, 1, '2019-11-05 16:34:34'),
('274e97', 'Đồng hồ Vintage 58cm mã 01', '8d9c', '1d7827d5c7d74309b16e0722dfec4590-300x300.jpg', 939000, 28, 1, '2019-11-05 16:34:34'),
('27a595', 'Đồng hồ vintage 23 hình chim sẻ', '8d9c', 'be46d51a9c3045b3b18a891587d855ce-300x300.jpg', 690000, 28, 1, '2019-11-05 16:34:34'),
('2b0ffd', 'Cặp lông mi gỗ', '19c5', 'dong-ho-bao-thuc-ngoi-nha-thach-cao-02-1-300x300.jpg', 75000, 33, 1, '2019-11-05 16:34:34'),
('2e0d23', 'Hoa hồng xốp hoa hồng màu hồng', '2ec0', 'gom-su-meo-trong-cay-trang-tri-300x300.jpg', 25000, 20, 1, '2019-11-05 16:34:34'),
('2fa060', 'Sọt rác lớn hươu hiện đại màu đen', '19c5', 'xe-hoi-dong-ho-vintage-do-trang-tri-gia-co-phong-cach-vintage-300x300.jpg', 175000, 10, 1, '2019-11-05 16:34:34'),
('2fbdc9', 'Tranh treo tường dụng cụ làm bếp', 'cf08', '1cb03c2cdbb9483c8fb93dda31bbee42-300x300.jpg', 275000, 20, 1, '2019-11-05 16:34:35'),
('2ffa0b', 'Đồng hồ hiện đại hình chim sẻ', '8d9c', '9fded41840cb400abbd9f6729a7c55b6-300x300.jpg', 625000, 13, 1, '2019-11-05 16:34:34'),
('31552a', 'Tranh thiếc vintage tháp bigben 20 x 30cm', 'cf08', '652911bdfd5b41cfbaf36400427115f1-300x300.jpg', 495000, 14, 1, '2019-11-05 16:34:35'),
('34bcb5', 'Đuốc lửa gốm sứ xanh hiện đại', '19c5', 'bo-3-moc-treo-do-bien-phong-cach-dia-trung-hai-300x300.jpg', 245000, 18, 1, '2019-11-05 16:34:34'),
('34e3ea', 'Tranh giấy phong cách vintage hình xe hơi', 'cf08', '8444d60609674008a91920179187cd7e-300x300.jpg', 850000, 15, 1, '2019-11-05 16:34:35'),
('34e85e', 'Đồng hồ báo thức ngôi nhà nhỏ', '19c5', 'ngon-hai-dang-30cm-do-trang-tri-bien-phong-cach-dia-trung-hai-300x300.jpg', 295000, 18, 1, '2019-11-05 16:34:34'),
('35cca4', 'Kệ sắt đen 3 ô treo tường', '19c5', 'bo-gom-su-hien-dai-mat-nguoi-mau-den-dep-doc-la-gom-su-trang-tri-1-300x300.jpg', 79000, 27, 1, '2019-11-05 16:34:34'),
('362aae', 'Đồng hồ đè lồng đỏ nghệ thuật quả lắc', '8d9c', 'd48dff63a0b1466da624b7f962ac8096-300x300.jpg', 115000, 31, 1, '2019-11-05 16:34:34'),
('3c8f91', 'Tranh sắt vải vintage hình hươu cao cổ', 'cf08', '0c6e9bbe6b5b4ecc821583e652864691-300x300.jpg', 335000, 7, 1, '2019-11-05 16:34:35'),
('3cc9dc', 'Người trắng hát - ca sĩ trắng', '19c5', 'ngua-trang-tri-tu-ke-qua-tang-tan-gia-cao-cap-sang-trong-300x300.jpg', 14000, 27, 1, '2019-11-05 16:34:34'),
('3e0f04', 'Đồng hồ mặt trời vàng', '8d9c', '062786912a6f486e8762f8a7b2aba983-300x300.jpg', 495000, 30, 1, '2019-11-05 16:34:34'),
('3ec636', 'Trái tim sắt trắng', 'cf08', 'trai-tim-sat-mau-trang-treo-anh-trang-tri-2-300x300.jpg', 199000, 7, 1, '2019-11-05 16:34:34'),
('3fba06', 'Đồng hồ vintage 22 hình quả nho', '8d9c', '3964117270_1898523357-300x300.jpg', 255000, 5, 1, '2019-11-05 16:34:34'),
('41920b', 'Hộp cafedo nhỏ màu gỗ', '19c5', '898671138e964efe9839e57e8cf6e34d-300x300.jpg', 88000, 9, 1, '2019-11-05 16:34:34'),
('4550fc', 'Đồng hồ thiên thần', '8d9c', '135a391769694a929277b6f4d7b60db7-300x300.jpg', 939000, 14, 1, '2019-11-05 16:34:34'),
('463d1f', 'Đồng hồ đầu hươu gỗ quả lắc 02', '8d9c', 'dong-ho-treo-tuong-hien-dai-trang-04-1-300x300.jpg', 265000, 22, 1, '2019-11-05 16:34:34'),
('46f965', 'Người đen thổi kèn saxophone nghệ thuật', '19c5', 'be-trai-hai-quan-ngoi-tren-bong-do-trang-tri-chu-de-bien-300x300.jpg', 209000, 11, 1, '2019-11-05 16:34:34'),
('48a0fb', 'Bé trai hải quân ngồi quả bóng', '19c5', '700060966a874f7ea17bdb12a28b8ec9-300x300.jpg', 239000, 13, 1, '2019-11-05 16:34:34'),
('49229c', 'Tranh gỗ Vintage Gas last change', 'cf08', '5426c3ef4dd741849e59f6995e804e83-300x300.jpg', 495000, 33, 1, '2019-11-05 16:34:35'),
('498774', 'Tranh thép mẫu mới hình hồng hạc', 'cf08', 'f8d4b6aebe754a03932f9b1012012b9f-300x300.jpg', 495000, 10, 1, '2019-11-05 16:34:35'),
('498922', 'Thơm gốm sứ vàng hiện đại', '19c5', 'mo-hinh-coi-xoay-gio-vintage-co-den-300x300.jpg', 209000, 33, 1, '2019-11-05 16:34:34'),
('4a96d5', 'Đồng hồ đầu hươu nhỏ nghệ thuật màu trắng', '8d9c', 'c49c985f28ca418ab56f11c28a2c79bf-300x300.jpg', 939000, 14, 1, '2019-11-05 16:34:34'),
('4bc656', 'Đồng hồ vintage 22', '8d9c', '294dd56aeab44fbeac25b0febee00984-300x300.jpg', 948000, 22, 1, '2019-11-05 16:34:34'),
('5052a9', 'Đồng hồ cánh hoa mica', '8d9c', 'fcd99bc04e214824be1abdca7e25c3ff-300x300.jpg', 265000, 26, 1, '2019-11-05 16:34:34'),
('52c76a', 'Người đen kéo đàn violon - cô gái đàn vĩ cầm', '19c5', 'gia-sach-vintage-dung-do-sieu-dep-do-trang-tri-phong-cach-co-dien-gia-co-300x300.jpg', 79000, 32, 1, '2019-11-05 16:34:34'),
('555c4b', 'Đồng hồ vintage 14 Paris France', '8d9c', '4e1589f7eb684b5bae824f844628bb7c-300x300.jpg', 690000, 12, 1, '2019-11-05 16:34:34'),
('55981d', 'Lịch gỗ giấy để bàn màu xanh vintage', '19c5', '89b28e87496b445e9082354204d2622d-300x300.jpg', 245000, 24, 1, '2019-11-05 16:34:34'),
('55c2e0', 'Đồng hồ cành cây chim sẻ mica', '8d9c', '7831aaaf3517449f8846bed8aa2783de-300x300.jpg', 206000, 26, 1, '2019-11-05 16:34:34'),
('59a1ed', 'Đồng hồ vintage 27 Paris', '8d9c', 'dong-ho-treo-tuong-dep-nghe-thuat-doc-dao-hinh-dau-huou-qua-tang-tan-gia-dep-y-nghia-2-300x300.png', 215000, 30, 1, '2019-11-05 16:34:34'),
('5b9bf7', 'Đồng hồ vintage 26 Hoa Lavender', '8d9c', 'e65c114df7c34b7f88e19f8e481a51f9-300x300.jpg', 215000, 30, 1, '2019-11-05 16:34:34'),
('5c0110', 'Tranh thiếc vintage Câu nói ý nghĩa Life Plans', 'cf08', '2eee6bb45dad43eb8b68a1e6f96557b8-300x300.jpg', 495000, 23, 1, '2019-11-05 16:34:35'),
('5d748a', 'Tranh treo tường nghệ thuật ILU', 'cf08', '2e1fdfd370a044d9b8c097ebde313b03-300x300.jpg', 935000, 17, 1, '2019-11-05 16:34:35'),
('5eec58', 'Đồng hồ công đuôi dài rũ xuống vàng 05', '8d9c', 'fc702ed6dc66469ba8bc39c82975ff25-300x300.jpg', 206000, 17, 1, '2019-11-05 16:34:34'),
('5f300a', 'Chậu xi măng lục giác đen nhỏ', '2ec0', '1813208d58cc49cf9616276dff4fecc3-300x300.jpg', 28000, 26, 1, '2019-11-05 16:34:34'),
('5f3e9c', 'Tranh treo tường Now', 'cf08', 'dc10523cc26f44028b43332c9fdf4d79-300x300.jpg', 695000, 24, 1, '2019-11-05 16:34:35'),
('6072d3', 'Đồng hồ hiện đại hình hươu đen', '8d9c', 'e8eeabadd56e495b92231207a6a7fdce-300x300.jpg', 215000, 11, 1, '2019-11-05 16:34:34'),
('62fe6c', 'Ngựa rễ cây', '19c5', 'cap-co-dau-chu-re-khieu-vu-dinh-da-sang-trong-phong-cach-chau-au-300x300.jpg', 289000, 29, 1, '2019-11-05 16:34:34'),
('66eaf1', 'Tranh treo tường hình ngựa vằn thổi bóng', 'cf08', '27ef96e7b9d6410b9b7f0fbbb3eb44f2-300x300.jpg', 95000, 5, 1, '2019-11-05 16:34:35'),
('689250', 'Hoa hồng xốp handmade bông lớn màu hồng', '2ec0', 'hoa-hong-nhung-bong-lon-mau-xanh-trang-tri-300x300.jpg', 249000, 27, 1, '2019-11-05 16:34:34'),
('6d7776', 'Tranh cát', 'cf08', 'b6911e4079cd45f992766b02fe0ea137-300x300.jpg', 335000, 14, 1, '2019-11-05 16:34:35'),
('6dcfb1', 'Đồng hồ cát đen tròn 03', '19c5', '0bb5d003c38c40249ddcd5101f2d8ff0-300x300.jpg', 265000, 32, 1, '2019-11-05 16:34:34'),
('711b8f', 'Đồng hồ vintage 24 I Love LONDON hình xe đạp', '8d9c', '061b4f9dd79d40d7b7d0051fffba64ec-300x300.jpg', 545000, 25, 1, '2019-11-05 16:34:34'),
('714804', 'Tranh thêu chim sẻ và hoa đào', 'cf08', 'e02024c6c494481089939c8aa8390c3c-300x300.jpg', 495000, 11, 1, '2019-11-05 16:34:35'),
('71cfd4', 'Đồng hồ số cách điệu size nhỏ', '8d9c', 'dong-ho-bao-thuc-ngoi-nha-thach-cao-02-1-300x300.jpg', 255000, 20, 1, '2019-11-05 16:34:34'),
('7446e2', 'Đồng hồ để bàn báo thức ếch vương miện', '8d9c', '59838545ccad4af3802795ed169ececd-300x300.jpg', 305000, 13, 1, '2019-11-05 16:34:34'),
('7460e0', 'Đồng hồ treo tường hiện đại số la mã hình thoi', '8d9c', '5a653c5dd3e14b288b374d25f3c73495-300x300.jpg', 215000, 8, 1, '2019-11-05 16:34:34'),
('779bd7', 'chậu hoa Life màu hồng', '2ec0', 'gio-sat-home-treo-hoa-trang-tri-tuong-nha-quan-cafe-dep-300x300.jpg', 169000, 26, 1, '2019-11-05 16:34:34'),
('78571a', 'Đồng hồ hiện đại mặt gỗ hình thoi', '8d9c', 'b3bf4b25c37c4c268d5f1a81384127a4-300x300.jpg', 215000, 17, 1, '2019-11-05 16:34:34'),
('7966f7', 'Tranh giấy phong cách vintage hình cây cô đơn', 'cf08', 'c4f9a8f367024caf9e8b352db0c0fdbb-300x300.jpg', 335000, 12, 1, '2019-11-05 16:34:35'),
('7c571d', 'Đồng hồ mica bướm', '8d9c', 'ad429a2b6e6a4c8baa799279f1bbfceb-300x300.jpg', 265000, 10, 1, '2019-11-05 16:34:34'),
('7d9711', 'Bộ gia đình hải quân gỗ ngồi', '19c5', 'gom-su-meo-trong-cay-trang-tri-300x300.jpg', 385000, 24, 1, '2019-11-05 16:34:34'),
('7eb37f', 'Đèn bão đi biển vintage trang trí màu xanh', '19c5', '63122eba661d4b37be79c6e888a992fb-300x300.jpg', 215000, 28, 1, '2019-11-05 16:34:34'),
('7fbb64', 'Gia đình mèo 3 xanh hồng gốm sứ phong cách nordic', '19c5', '0c8a14c036504efa9441df689c368d30-300x300.jpg', 265000, 24, 1, '2019-11-05 16:34:34'),
('80d9b0', 'Bộ 2 bé hải quân ngồi size nhỏ', '19c5', 'cap-co-dau-chu-re-khieu-vu-dinh-da-sang-trong-phong-cach-chau-au-1-300x300.jpg', 195000, 18, 1, '2019-11-05 16:34:34'),
('817b33', 'Thơm vàng size trung', '19c5', '759d5df7c3f04cada24e0ac709a2593a-300x300.jpg', 99000, 23, 1, '2019-11-05 16:34:34'),
('83bfb9', 'Tranh treo tường phong cảnh nghệ thuật đẹp', 'cf08', 'b8c38d39803c40a4a9032f545286a40f-300x300.jpg', 195000, 14, 1, '2019-11-05 16:34:35'),
('83c808', 'Giỏ sắt treo hoa Home trang trí tường', '2ec0', '8916010ba9734cb18793ece22e52ff14-300x300.jpg', 169000, 13, 1, '2019-11-05 16:34:34'),
('8452', 'z', '19c5', '', 0, 1, 1, '2019-11-05 23:28:42'),
('8496e8', 'Tranh gỗ trắng đầu hươu lớn', 'cf08', '837cac3f03b44cb289f543d9d3011bc7-300x300.jpg', 495000, 33, 1, '2019-11-05 16:34:35'),
('862c8f', 'Chậu cây xi măng totoro', '2ec0', '6890f10d2bd64c52a34759aefcd9e51e-300x300.jpg', 145000, 29, 1, '2019-11-05 16:34:34'),
('873ed7', 'Lồng nến cao treo màu đen', '19c5', 'cd704347275249bba7186f307d8b5776-300x300.jpg', 365000, 8, 1, '2019-11-05 16:34:34'),
('877036', 'Tranh sắt vải vintage hình linh dương', 'cf08', '5c6290840b864ddfa3137225be925ef1-300x300.jpg', 335000, 7, 1, '2019-11-05 16:34:35'),
('87dec6', 'Đồng hồ để bàn báo thức thỏ màu xanh', '8d9c', '448250cdb91b4ef98fd5d32639cc27c5-300x300.png', 835000, 20, 1, '2019-11-05 16:34:34'),
('88941d', 'Bộ chữ love vải bố', '19c5', '07127c00f64a49938f1378b04907eee1-300x300.jpg', 125000, 19, 1, '2019-11-05 16:34:34'),
('8a27f1', 'Chậu chấm bi xương rồng', '19c5', '46d06fee590945cdb3dd1f9a3f501c13-300x300.jpg', 215000, 26, 1, '2019-11-05 16:34:34'),
('8a364f', 'Đồng hồ quả lắc chim sẻ màu vàng', '8d9c', '92ceec1c3af3473f9b7bcb6aea8593b2-300x300.jpg', 939000, 32, 1, '2019-11-05 16:34:34'),
('8ae26c', 'Miếng gỗ lót lớn', '19c5', 'cf5cd6877fba40528ba7eadec3a39179-300x300.jpg', 70000, 28, 1, '2019-11-05 16:34:34'),
('8d649e', 'Đồng hồ Vintage 58cm mã 05', '8d9c', '898671138e964efe9839e57e8cf6e34d-300x300.jpg', 215000, 16, 1, '2019-11-05 16:34:34'),
('8e4213', 'Đồng hồ Vintage 58cm mã 04', '8d9c', 'b8d924f6d9f14217bc9f5e4a4f6f2797-300x300.jpg', 215000, 22, 1, '2019-11-05 16:34:34'),
('8ff4b0', 'Bộ đựng gia vị bếp khay gỗ bỏ nắp', '19c5', 'd3b2a07c355246b789426ff3eea47313-300x300.jpg', 385000, 30, 1, '2019-11-05 16:34:34'),
('92d8b8', 'Tượng người không nhìn', '19c5', 'ff6301a6d1ea4ef3bf5bd5328791b097-300x300.jpg', 385000, 10, 1, '2019-11-05 16:34:34'),
('9529c0', 'Tranh vải tropical', 'cf08', '12af33c23dce4e7eb5f654e99696aada-300x300.jpg', 335000, 12, 1, '2019-11-05 16:34:35'),
('97be28', 'Đồng hồ vintage 26 Hoa Lavender Tím', '8d9c', '396033cacfd847bda54a956e04bf3396-300x300.jpg', 215000, 31, 1, '2019-11-05 16:34:34'),
('97bff4', 'Thiên nga gốm sứ hoa hồng lớn 02', '19c5', 'bo-gom-su-hien-dai-mat-nguoi-mau-den-dep-doc-la-gom-su-trang-tri-300x300.jpg', 209000, 11, 1, '2019-11-05 16:34:34'),
('9bbc52', 'Đồng hồ hiện đại hình lông vũ', '8d9c', 'b500c0000c3a4a4cabe3053d2126d906-300x300.jpg', 215000, 13, 1, '2019-11-05 16:34:34'),
('9c9cec', 'Chậu xi măng lục giác xanh nhỏ', '2ec0', 'hoa-dao-gia-mau-hong-trang-tri-nha-dep-ngay-tet-300x300.jpg', 28000, 20, 1, '2019-11-05 16:34:34'),
('9e9e56', 'Bảng gỗ thủy tinh Home wherever 02', '2ec0', 'hoa-cuc-canh-buom-mau-trang-trang-tri-nha-quan-cafe-tra-sua-hoa-gia-dep-trang-tri-300x300.jpg', 245000, 33, 1, '2019-11-05 16:34:34'),
('9eb703', 'Bình hoa thủy tinh Pafume', '2ec0', 'bo-gom-su-hien-dai-mat-nguoi-mau-den-dep-doc-la-gom-su-trang-tri-300x300.jpg', 180000, 32, 1, '2019-11-05 16:34:34'),
('a145c1', 'Bó hoa mẫu đơn cam hồng', '2ec0', 'bo-hoa-mau-don-mau-cam-hoa-gia-dep-trang-tri-300x300.jpg', 18000, 10, 1, '2019-11-05 16:34:34'),
('a1caaf', 'Chậu xi măng lục giác xanh lớn', '2ec0', 'binh-hoa-hong-canh-cao-trang-tri-dep-300x300.jpg', 28000, 7, 1, '2019-11-05 16:34:34'),
('a32b83', 'Tay ngọc nhỏ gốm sứ', '19c5', 'bo-cay-vai-bo-love-trang-tri-nha-sieu-xinh-300x300.jpg', 125000, 27, 1, '2019-11-05 16:34:34'),
('a59131', 'Đồng hồ tay lái tàu biển nhỏ', '8d9c', '7d30cf3df09442848c62fca2f917783d-300x300.jpg', 948000, 19, 1, '2019-11-05 16:34:34'),
('a5eb62', 'Máy may vintage', '19c5', '737bd76783f54b178331d08c32d9ff52-300x300.jpg', 125000, 27, 1, '2019-11-05 16:34:34'),
('a63985', 'Đồng hồ cát đen tròn 03', '8d9c', 'b6e8ca85e51c45f49ce516e5ae8a05cb-300x300.jpg', 215000, 33, 1, '2019-11-05 16:34:34'),
('a656fb', 'Bộ 4 heo nhỏ để xe hơi', '19c5', 'kich-thuoc-sot-rac-hien-dai-size-lon-300x300.jpg', 195000, 29, 1, '2019-11-05 16:34:34'),
('a66ab0', 'Bình hoa Garden hoa màu hồng', '2ec0', '393f521468924c2eb577f5471a249e1f-300x300.jpg', 122000, 30, 1, '2019-11-05 16:34:34'),
('a7b96d', 'Đồng hồ mặt trời đen', '8d9c', '5235564004474a04b0561db8605cd017-300x300.jpg', 495000, 22, 1, '2019-11-05 16:34:34'),
('a86d4b', 'Tranh treo tường hình ngựa trắng đen nghệ thuật', 'cf08', '3586ac5598dd40f2b3dbbbf262e7a6ad-300x300.jpg', 95000, 22, 1, '2019-11-05 16:34:35'),
('a927fb', 'Thuyền xanh trang trí', '19c5', 'hop-dung-khan-giay-hinh-co-anh-phong-cach-vintage-co-dien-300x300.jpg', 14000, 8, 1, '2019-11-05 16:34:34'),
('ac5f75', 'Cối xoay gió đèn', '19c5', 'f3785b6cc56941fd97b89429ef733890-300x300.jpg', 239000, 28, 1, '2019-11-05 16:34:34'),
('ac65ba', 'Hoa oải hương vải bố nhỏ màu vàng', '2ec0', 'hoa-bi-trang-trang-tri-cam-hoa-sieu-dep-300x300.jpg', 169000, 25, 1, '2019-11-05 16:34:34'),
('ae44a9', 'Tranh lọ thủy tinh cắm hoa', 'cf08', '4da84a7202b34558bf6a63f680c1e458-300x300.jpg', 335000, 12, 1, '2019-11-05 16:34:35'),
('b07e10', 'Đồng hồ vintage Olive mã 11', '8d9c', '4a54cdbfe115405cb639dd150543fe0e-300x300.jpg', 625000, 25, 1, '2019-11-05 16:34:34'),
('b26c04', 'Thỏ tròn nhỏ', '19c5', 'bo-gia-dinh-hai-quan-ngoi-do-trang-tri-chu-de-bien-300x300.jpg', 125000, 19, 1, '2019-11-05 16:34:34'),
('b4d2a4', 'Tranh gỗ vị trí lịch sử Rome', 'cf08', '2d690c1bcf46405fa5c8c1bc24a1175c-300x300.jpg', 695000, 7, 1, '2019-11-05 16:34:35'),
('b52fb3', 'Chậu cây xi măng hình gấu', '2ec0', 'bc61403f51904a5fb18da84946fd5147-300x300.jpg', 65000, 31, 1, '2019-11-05 16:34:34'),
('b57635', 'Móc câu treo trang trí chủ đề biển phong cách địa ', '19c5', 'e545e3a56c9345febdbf6632a09cc85b-300x300.jpg', 10000, 26, 1, '2019-11-05 16:34:34'),
('b5b1d1', 'Đồng hồ vintage 19', '8d9c', 'CC30-1-300x300.jpg', 948000, 14, 1, '2019-11-05 16:34:34'),
('b86f49', 'đồng hồ đầu hươu', '8d9c', '95972d52878b4f8a844f4019e564efe8-300x300.jpg', 115000, 33, 1, '2019-11-05 16:34:34'),
('b8ef12', 'Thảm voi treo tường', 'cf08', 'c13e4dbd19cd4889aec48fc6ce1fc1aa-300x300.jpg', 645000, 21, 1, '2019-11-05 16:34:34'),
('b99997', 'Cây gỗ xanh hoa lá độc đáo treo tường trang trí qu', '19c5', 'bo-gom-su-trang-tri-phong-khach-dep-gom-su-ca-chep-300x300.jpg', 155000, 9, 1, '2019-11-05 16:34:34'),
('bc2450', 'Đồng hồ hiện đại hình nai', '8d9c', 'VTB05-300x300.jpg', 625000, 10, 1, '2019-11-05 16:34:34'),
('be5fbe', 'Xe hơi Đồng hồ vintage', '8d9c', 'hinh-khach-review-dong-ho-dau-huou-qua-lac-treo-thuc-te-300x300.jpg', 695000, 24, 1, '2019-11-05 16:34:34'),
('be8b56', 'Đồng hồ hiện đại mặt gỗ hình tròn', '8d9c', 'f1e10d0287b44b18a0150e0e79ae56fc-300x300.jpg', 215000, 18, 1, '2019-11-05 16:34:34'),
('bf3eee', 'Quả địa cầu nhỏ', '19c5', '0c23874ea31f46469de64eb7c36523e1-300x300.jpg', 195000, 24, 1, '2019-11-05 16:34:34'),
('bf85f2', 'Đồng hồ cánh hoa quả lắc màu xanh', '8d9c', 'a8496a8e807e4fb587d76eda98340663-300x300.jpg', 206000, 28, 1, '2019-11-05 16:34:34'),
('c06d2b', 'Gương 1 chân màu đen', '19c5', 'ea4a0a55fef24a53bac929f04f19f733-300x300.jpg', 255000, 21, 1, '2019-11-05 16:34:34'),
('c1b2c3', 'Chậu Grow sen đá trang trí', '19c5', 'bo-chau-cay-xuong-rong-sen-da-grow-trang-tri-300x300.jpg', 155000, 22, 1, '2019-11-05 16:34:34'),
('c21452', 'Bảng gỗ thủy tinh treo tường đơn giản độc đáo tran', '2ec0', 'bo-3-bang-go-thuy-tinh-bon-mua-xuan-ha-thu-dong-trang-tri-tuong-nha-quan-cafe-spa-hien-dai-300x300.jpg', 125000, 8, 1, '2019-11-05 16:34:34'),
('c241d8', 'Lưới sắt 20 x 20 nhỏ', '19c5', 'df50f254a37046618a5b5302850131ca-300x300.jpg', 88000, 11, 1, '2019-11-05 16:34:34'),
('c247a3', 'Thơm gốm sứ trắng hiện đại', '19c5', 'bo-heo-love-de-xe-hoi-trang-tri-sieu-xinh-300x300.jpg', 79000, 14, 1, '2019-11-05 16:34:34'),
('c38656', 'Đồng hồ cành hoa quả lắc màu đen', '8d9c', '23adcd3973bb4116a4e5f027bbcd06b2-300x300.jpg', 759000, 21, 1, '2019-11-05 16:34:34'),
('c47fc7', 'Đồng hồ sách điện thoại vintage xanh', '8d9c', '166fe02e98e54855ac1cef0b5f011020-300x300.jpg', 265000, 33, 1, '2019-11-05 16:34:34'),
('c5225f', 'Sư tử đá', '19c5', 'moc-treo-do-phong-cach-bien-dia-trung-hai-300x300.jpg', 175000, 31, 1, '2019-11-05 16:34:34'),
('c55f01', 'Đồng hồ đầu hươu gỗ xám xanh', '8d9c', '495f39928c1e4db7922b81250da12cc5-300x300.jpg', 215000, 16, 1, '2019-11-05 16:34:34'),
('c744e9', 'Hoa bông gòn khô trang trí', '2ec0', '1e098ce026254628b0f1ebc429021b18-300x300.jpg', 18000, 34, 1, '2019-11-05 16:34:34'),
('ca6bee', 'Tháp epphen 15cm', '19c5', '7b20609be65e46bc891894981061cc7d-300x300.jpg', 365000, 32, 1, '2019-11-05 16:34:34'),
('cad841', 'Đồng hồ báo thức ngôi nhà nhỏ', '8d9c', 'dong-ho-treo-tuong-hien-dai-trang-02-1-300x300.jpg', 265000, 18, 1, '2019-11-05 16:34:34'),
('cbcbf8', 'Tranh gỗ America\'s Highway', 'cf08', '63c6bc0a92cc4d76abc2ed488a9c50d1-300x300.jpg', 495000, 29, 1, '2019-11-05 16:34:35'),
('ccc256', 'Cây cao vải bố màu xanh', '19c5', '3964117270_1898523357-300x300.jpg', 75000, 17, 1, '2019-11-05 16:34:34'),
('cce659', 'Gương 2 chân màu đen', '19c5', 'qua-dia-cau-vintage-do-trang-tri-gia-co-phong-cach-vintage-300x300.jpg', 209000, 13, 1, '2019-11-05 16:34:34'),
('ceb6b2', 'Đồng hồ Vintage 58cm mã 02', '8d9c', 'f6e9ba93be6449e89e45fee51908b237-300x300.jpg', 215000, 34, 1, '2019-11-05 16:34:34'),
('cebce6', 'Miếng gỗ lót nhỏ', '19c5', 'cay-go-la-sen-da--300x300.jpg', 385000, 20, 1, '2019-11-05 16:34:34'),
('d0f8a2', 'Mica đồng hồ cá', '8d9c', '9fd75c3f5815445e9f22a9baae31e086-300x300.jpg', 625000, 21, 1, '2019-11-05 16:34:34'),
('d1b1f6', 'Bó hoa hồng màu hồng 12 bông 02', '2ec0', 'hoa-oai-huong-vai-bo-mau-tim-trang-tri-300x300.jpg', 180000, 12, 1, '2019-11-05 16:34:34'),
('d1f75a', 'Tranh treo tường ILU và hoa bồ công anh', 'cf08', '576c5956f4d84a1da3716d24a4d644ae-300x300.jpg', 95000, 33, 1, '2019-11-05 16:34:35'),
('d35a1e', 'Đồng hồ số cách điệu size lớn', '8d9c', 'dong-ho-nghe-thuat-hinh-huou-300x300.jpg', 215000, 26, 1, '2019-11-05 16:34:34'),
('d4ee3f', 'Chậu xương rồng màu hồng young for you size nhỏ se', '2ec0', 'fb7615635abc4751a21b9bf6f15dffb9-300x300.jpg', 169000, 17, 1, '2019-11-05 16:34:34'),
('d77a40', 'Người đen xe đạp', '19c5', 'bo-gia-dinh-hai-quan-ngoi-do-trang-tri-chu-de-bien-do-trang-tri-nha-tu-ke-dep-1-300x300.jpg', 79000, 6, 1, '2019-11-05 16:34:34'),
('d92ded', 'Đồng hồ Vintage 58cm mã 06', '8d9c', 'df50f254a37046618a5b5302850131ca-300x300.jpg', 215000, 13, 1, '2019-11-05 16:34:34'),
('da3d5c', 'Tranh sắt vải vintage hình dê', 'cf08', 'fd78c5fdcc5e46b5adab2f7d9fa5a090-300x300.jpg', 335000, 32, 1, '2019-11-05 16:34:35'),
('dba637', 'Hoa bi nhựa màu trắng 01', '2ec0', '7c6e8a21e68f495892437b6c44462f65-300x300.jpg', 169000, 25, 1, '2019-11-05 16:34:34'),
('de9c67', 'Cặp gốm sứ cá chép lớn trắng đen', '19c5', '91f237906e65478c8f945854ad0565f1-300x300.jpg', 155000, 33, 1, '2019-11-05 16:34:34'),
('e12acc', 'Xe hơi Đồng hồ vintage', '19c5', 'ngon-hai-dang-do-trang-tri-chu-de-bien-bang-go-phong-cach-dia-trung-hai-300x300.jpg', 10000, 7, 1, '2019-11-05 16:34:34'),
('e3ae3f', 'Đồng hồ hiện đại ký hiệu sừng hươu', '8d9c', '0cfcf273a3be4bc492044b78d4d9408e-300x300.jpg', 215000, 15, 1, '2019-11-05 16:34:34'),
('e534fe', 'Tranh treo tường phong cảnh bến tàu', 'cf08', '693095bd9f0f47f8b97dc1d9ee890c19-300x300.jpg', 165000, 16, 1, '2019-11-05 16:34:34'),
('e5f81a', 'Đồng hồ mặt trời mica', '8d9c', 'bdd68900831d456695e88154a422f091-300x300.jpg', 835000, 24, 1, '2019-11-05 16:34:34'),
('e64acb', 'Tranh giấy phong cách vintage khinh khí cầu', 'cf08', '1f42ee9a7a33403c9a2929bf0df8e175-300x300.jpg', 850000, 16, 1, '2019-11-05 16:34:35'),
('e90522', 'Đồng hồ vintage 25 Good Time With Good Friend', '8d9c', '1368e4ac5e1c4519b4ed10131eac5ab9-300x300.jpg', 545000, 15, 1, '2019-11-05 16:34:34'),
('ea93ff', 'Hộp Giả sách vintage Hamlet', '19c5', '3a95cabde075473c8dea9adaccfbfa61-300x300.jpg', 195000, 11, 1, '2019-11-05 16:34:34'),
('eac169', 'Đèn bão đi biển vintage trang trí màu trắng', '19c5', '6b95c3308639447e81dfccb10fdf43b1-300x300.jpg', 215000, 9, 1, '2019-11-05 16:34:34'),
('eb8ffb', 'Đồng hồ mica coffee trà sữa', '8d9c', '4f94b94835d6459a8bda3b9ec678344a-300x300.jpg', 765000, 7, 1, '2019-11-05 16:34:34'),
('ec97e0', 'Đồng hồ hiện đại hình hươu lục giác', '8d9c', 'e90ec4836b2e4609a13e0f36ce0fdf14-300x300.jpg', 155000, 25, 1, '2019-11-05 16:34:34'),
('eea389', 'Chậu hình mèo trồng cây cắm hoa xám', '19c5', 'bo-cay-go-hoa-treo-tuong-trang-tri-doc-dao-03-300x300.jpg', 155000, 12, 1, '2019-11-05 16:34:34'),
('eeebff', 'Lịch gỗ xúc sắc để bàn màu đen', '19c5', '169263456b25448bb41c0055b5bd7b4c-300x300.jpg', 245000, 22, 1, '2019-11-05 16:34:34'),
('f056d4', 'Bộ 2 mặt người đen không kèm hoa', '19c5', 'ee97994b345d4a218b298d2cf124297b-300x300.jpg', 239000, 29, 1, '2019-11-05 16:34:34'),
('f2170b', 'Chậu chấm bi sen đá nhật nguyệt', '19c5', 'ed4dfa0383204f3bb20efec5dab549f4-300x300.jpg', 545000, 7, 1, '2019-11-05 16:34:34'),
('f260f6', 'Tranh giấy phong cách vintage phong cảnh tháp bigb', 'cf08', 'f094bfd154434f35915bfedb5d654547-300x300.jpg', 850000, 30, 1, '2019-11-05 16:34:35'),
('f62ca5', 'Đồng hồ vintage 01', '8d9c', 'CC30-300x300.jpg', 948000, 12, 1, '2019-11-05 16:34:34'),
('f7239f', 'Đồng hồ cát đen vuông 01', '19c5', 'fd200fa87a3341fe8225e05bab91b2c1-300x300.jpg', 115000, 9, 1, '2019-11-05 16:34:34'),
('f77783', 'Đồng hồ hiện đại hình phong cảnh trừu tượng', '8d9c', '20ce0c9cb6b244a2a4672a50004466ba-300x300.jpg', 215000, 15, 1, '2019-11-05 16:34:34'),
('f79cbd', 'Đàn sách đồng hồ vintage', '8d9c', '955a32bd0daa41aea5e81330eb93ffa8-300x300.jpg', 265000, 15, 1, '2019-11-05 16:34:34'),
('f976db', 'Tranh gỗ trắng đầu hươu nhỏ', 'cf08', '062ee5c89bad4110805a020e8041f7b4-300x300.jpg', 495000, 25, 1, '2019-11-05 16:34:35'),
('fa72b8', 'Lịch gỗ giấy để bàn đen', '19c5', 'f2d2f059c89e4b52b86ead1ac880db6d-300x300.jpg', 99000, 25, 1, '2019-11-05 16:34:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `customer_id` (`customer_id`) USING BTREE;

--
-- Indexes for table `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`order_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
