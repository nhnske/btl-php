<?php
require_once 'db.php';

function createCustomer($conn, $Name, $Address, $Phone, $Email)
{
    $cond = TRUE;
    while ($cond) {
        $Id = substr(uniqid(rand(10000, 99999), True), 0, 6);
        $cond = checkCustomerIdExists($conn, $Id);
    }
    db_query($conn, "INSERT INTO `customers`(`id`, `name`, `address`, `phone`, `email`) 
    VALUES ('$Id', '$Name', '$Address', '$Phone', '$Email')");
    return $Id;
}

function updateCustomer($conn, $Id, $Name, $Address, $Phone, $Email)
{
    db_query($conn, "UPDATE `customers`(`id`, `name`, `address`, `phone`, `email`) 
    VALUES ('$Id', '$Name', '$Address', '$Phone', '$Email')");
    return $Id;
}


function checkCustomerIdExists($conn, $id)
{
    $result = db_query($conn, "SELECT id FROM `customers` WHERE id='$id' LIMIT 0,1");
    if (mysqli_num_rows($result) > 0)  return TRUE;
    return FALSE;
}

function getCustomerFromId($conn, $id)
{
    $result = db_query($conn, "SELECT * FROM `customers` WHERE id='$id' LIMIT 0,1");
    return $result;
}
