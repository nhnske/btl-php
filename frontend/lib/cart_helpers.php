<?php

function addItem2Cart($id, $q)
{
    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    if (!array_key_exists('cart', $_SESSION)) $_SESSION['cart'] = array();
    if (!array_key_exists($id, $_SESSION['cart'])) {
        $key = array_search($id, $_SESSION['cart']);
        unset($_SESSION['cart'][$key]);
    }
    $_SESSION['cart'][$id] = array("quantity" => $q);
}

function getCartCount()
{
    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    if (!array_key_exists('cart', $_SESSION)) $_SESSION['cart'] = array();
    return count($_SESSION['cart']);
}

function emptyCart()
{
    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    $_SESSION['cart'] = array();
}