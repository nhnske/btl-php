<?php
require_once 'db.php';

function createOrder($conn, $CustomerId, $Products, $Note)
{
	print_r($Products);
	$cond = TRUE;
	while ($cond) {
		$Id = substr(uniqid(rand(10000, 99999), True), 0, 6);
		$cond = checkOrderIdExists($conn, $Id);
	}
	db_query($conn, "INSERT INTO `orders`(`id`, `customer_id`, `note`) 
	VALUES ('$Id', '$CustomerId', '$Note')");
	foreach (array_keys($Products) as $itemId) {
		$item = $Products[$itemId];
		db_query($conn, "INSERT INTO `orders_products`(`order_id`, `product_id`, `price`, `quantity`) 
		VALUES ('$Id', '" . $itemId . "', '". $item['price'] ."', '" . $item['quantity'] . "')");
	}
}

function getOrdersByCustomerId($conn, $id) {
	$result = db_query($conn, 
	"SELECT orders.id as order_id,customers.`name` as customer_name, customers.address  as customer_address, customers.phone as customer_phone,  customers.email as customer_email, orders.created_at as order_created_at,SUM(orders_products.price*orders_products.quantity) as sum_price,orders.created_at as order_time, orders.note as order_note,orders.status as order_status
	from  orders
	LEFT JOIN customers
	on orders.customer_id = customers.id
	 left JOIN orders_products 
	on orders.id = orders_products.order_id
	LEFT JOIN products
	on products.id = orders_products.product_id
	WHERE orders.customer_id='$id'
	GROUP BY order_id;");
	return $result;
}

function getProductsByOrderId($conn, $id) {
	$result = db_query($conn, 
	"SELECT `orders_products`.*, `orders_products`.`price` as op_price, `products`.*
	FROM `orders_products` 
		LEFT JOIN `products` ON `orders_products`.`product_id` = `products`.`id`
		WHERE `orders_products`.`order_id` = '$id';");
	return $result;
}

function checkOrderIdExists($conn, $id)
{
	$result = db_query($conn, "SELECT id FROM `orders` WHERE id='$id' LIMIT 0,1");
	if (mysqli_num_rows($result) > 0)  return TRUE;
	return FALSE;
}
