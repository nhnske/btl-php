<?php
    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    unset($_SESSION['customer']);
    header('Location: /project-php/frontend/user/redirect.php?mode=signout-success');
    exit();
