<?php
require("../lib/db.php");
require("../lib/customer_service.php");
require("../lib/user_service.php");


if (isset($_POST["signup"])) {
    $conn = db_connect();
    $CustomerId = createCustomer($conn, $_POST["name"], $_POST["address"], $_POST["phone"], $_POST["email"]);
    $result = createUser($conn, $_POST["username"], md5($_POST["psw"]), $CustomerId);
    mysqli_close($conn);
    header('Location: /project-php/frontend/user/redirect.php?mode=signup-success');
}
?>

<!DOCTYPE html>
<html>
<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    * {
        box-sizing: border-box;
    }

    /* Full-width input fields */
    input[type=text],
    input[type=tel],
    input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    /* Add a background color when the inputs get focus */
    input[type=text]:focus,
    input[type=tel]:focus,
    input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Set a style for all buttons */
    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    button:hover {
        opacity: 1;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
        padding: 14px 20px;
        background-color: #f44336;
    }

    /* Float cancel and signup buttons and add an equal width */
    .cancelbtn,
    .signupbtn {
        float: left;
        width: 50%;
    }

    /* Add padding to container elements */
    .container {
        padding: 16px;
    }

    /* The Modal (background) */
    .modal {
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: #f16564;
        padding-top: 50px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto;
        /* 5% from the top, 15% from the bottom and centered */
        border: 1px solid #888;
        width: 50%;
        /* Could be more or less, depending on screen size */
    }

    /* Style the horizontal ruler */
    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Clear floats */
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    /* Change styles for cancel button and signup button on extra small screens */
    @media screen and (max-width: 300px) {

        .cancelbtn,
        .signupbtn {
            width: 100%;
        }
    }
</style>

<body>

    <h2>Modal Signup Form</h2>

    <div id="id01" class="modal">
        <form class="modal-content" method="post">
            <div class="container">
                <h1>Đăng kí</h1>
                <p>Hãy điền vào mẫu sau để đăng kí tài khoản.</p>
                <hr>
                <label for="username"><b>Tên đăng nhập</b></label>
                <input type="text" placeholder="" name="username" required>

                <label for="psw"><b>Mật khẩu</b></label>
                <input type="password" placeholder="" name="psw" required>

                <label for="name"><b>Họ tên</b></label>
                <input type="text" placeholder="" name="name" required>

                <label for="address"><b>Địa chỉ</b></label>
                <input type="text" placeholder="" name="address" required>

                <label for="phone"><b>Số điện thoại</b></label>
                <input type="tel" placeholder="" name="phone" pattern="^(?:0|\(?\+84\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" required>

                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>

                <div class="clearfix">
                    <button type="button" onclick="location.href='/project-php/frontend/products/'" class="cancelbtn">Hủy</button>
                    <button type="submit" name="signup" class="signupbtn">Đăng kí</button>
                </div>
            </div>
        </form>
    </div>