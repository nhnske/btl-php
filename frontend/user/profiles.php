<?php
require("../lib/db.php");
require("../lib/customer_service.php");
require("../lib/user_service.php");

if (session_status() != PHP_SESSION_ACTIVE) session_start();
if (isset($_POST["update"])) {
    $conn = db_connect();
    updateUser(
        $conn,
        $_SESSION['customer']['username'],
        md5($_POST["psw"]),
        $_POST["name"],
        $_POST["address"],
        $_POST["phone"],
        $_POST["email"]
    );
    mysqli_close($conn);
    header('Location: /project-php/frontend/user/redirect.php?mode=update-success');
}
?>

<!DOCTYPE html>
<html>
<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    * {
        box-sizing: border-box;
    }

    /* Full-width input fields */
    input[type=text],
    input[type=tel],
    input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    /* Add a background color when the inputs get focus */
    input[type=text]:focus,
    input[type=tel]:focus,
    input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Set a style for all buttons */
    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    button:hover {
        opacity: 1;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
        padding: 14px 20px;
        background-color: #f44336;
    }

    /* Float cancel and signup buttons and add an equal width */
    .cancelbtn,
    .signupbtn {
        float: left;
        width: 50%;
    }

    /* Add padding to container elements */
    .container {
        padding: 16px;
    }

    /* The Modal (background) */
    .modal {
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: #f16564;
        padding-top: 50px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto;
        /* 5% from the top, 15% from the bottom and centered */
        border: 1px solid #888;
        width: 50%;
        /* Could be more or less, depending on screen size */
    }

    /* Style the horizontal ruler */
    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Clear floats */
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    /* Change styles for cancel button and signup button on extra small screens */
    @media screen and (max-width: 300px) {

        .cancelbtn,
        .signupbtn {
            width: 100%;
        }
    }
</style>

<body>

    <h2>Modal Signup Form</h2>

    <div id="id01" class="modal">
        <form class="modal-content" method="post">
            <div class="container">
                <h1>Thông tin tài khoản</h1>
                <p>Hãy sửa những thông tin cần thiết.</p>
                <hr>
                <?php
                $conn = db_connect();
                require_once("../lib/customer_service.php");
                if (session_status() != PHP_SESSION_ACTIVE) session_start();
                $customer = mysqli_fetch_assoc(getCustomerFromId($conn, $_SESSION['customer']['customer_id']));
                echo '
                    <label for="username"><b>Tên đăng nhập</b></label>
                    <input type="text" placeholder="" name="username" value="' . $_SESSION['customer']['username'] . '" disabled>

                    <label for="psw"><b>Mật khẩu</b></label>
                    <input type="password" placeholder="Chưa thay đổi" name="psw" pattern="(?=^.{8,}$)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">

                    <label for="name"><b>Họ tên</b></label>
                    <input type="text" placeholder="" name="name" value="' . $customer['name'] . '" required>

                    <label for="address"><b>Địa chỉ</b></label>
                    <input type="text" placeholder="" name="address" value="' . $customer['address'] . '" required>

                    <label for="phone"><b>Số điện thoại</b></label>
                    <input type="tel" placeholder="" name="phone" value="' . $customer['phone'] . '" pattern="^(?:0|\(?\+84\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" required>

                    <label for="email"><b>Email</b></label>
                    <input type="text" placeholder="" name="email" value="' . $customer['email'] . '" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                    ';
                ?>

                <div class="clearfix">
                    <button type="button" onclick="location.href='/project-php/frontend/products/'" class="cancelbtn">Hủy</button>
                    <button type="submit" name="update" class="signupbtn">Cập nhật thông tin</button>
                </div>
            </div>
        </form>
    </div>