<head>
    <?php include '../layouts/stylesheets.php'; ?>
</head>

<body>
    <?php include '../layouts/header.php'; ?>

    <div id="nasa-breadcrumb-site" class="bread">
        <div class="row">
            <div class="large-12 columns nasa-display-table">
                <div class="breadcrumb-row text-center" style="height:130px;">
                    <h2>Danh sách đơn hàng</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-top" style="min-height: 400px;">
        <div class="large-12 columns">
            <div class="large-8 columns side-padding">
                <form method="post">

                    <table class="table-border-bottom table-spaced-items table-center-headers">
                        <thead>
                            <tr>
                                <th class="order-time">Thời điểm đặt hàng</th>
                                <th class="order-note">Ghi chú đặt hàng</th>
                                <th class="order-total">Tổng giá đơn hàng</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            require("../lib/order_service.php");
                            $conn = db_connect();
                            $orders = getOrdersByCustomerId($conn, $_SESSION['customer']['customer_id']);
                            $i = 0;
                            $total = 0;
                            foreach ($orders as $order) {
                                $i += 1;
                                $total += $order['sum_price'];
                                echo '
                                <tr id="' . $order['order_id'] . '-row">
                                    <td class="order-time" data-title="time">
                                        <a href="order_detail.php?id='.$order['order_id'].'">
                                            ' . $order['order_time'] . '
                                        </a>
                                    </td>
                                    <td class="order-note" data-title="note">
                                        <span>' . $order['order_note'] . '</span>
                                    </td>
                                    <td class="order-total" data-title="total">
                                        <span>' . $order['sum_price'] . '</span>
                                    </td>
                                </tr>
                                ';
                            }
                            mysqli_close($conn);
                            ?>
                        </tbody>
                    </table>
                    <a href="/project-php/" class="button" style="width:100%">Quay lại trang chủ</a>
                </form>
            </div>


            <div class="large-4 columns gray-border" style="padding:20px">
                <div class="cart_totals">
                    <h5 class="heading-title"> Thông tin </h5>

                    <table class="table-spaced-items table-right-content table-border-bottom">
                        <tbody>
                            <tr>
                                <th>Số lượng đơn hàng</th>
                                <td><span><?php echo $i?> đơn</span></td>
                            </tr>

                            <tr class="order-total">
                                <th>Tổng giá trị</th>
                                <td><strong><span><?php echo number_format($total)?> VNĐ</span></strong> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
    <?php include '../layouts/footer.php'; ?>
</body>