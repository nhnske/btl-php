<?php
require_once("../lib/db.php");
require_once("../lib/order_service.php");
require_once("../lib/products_service.php");
require_once("../lib/cart_helpers.php");
?>

<head>
    <?php include '../layouts/stylesheets.php'; ?>
</head>

<body>
    <?php include '../layouts/header.php'; ?>
    <div id="nasa-breadcrumb-site" class="bread">
        <div class="row">
            <div class="large-12 columns nasa-display-table">
                <div class="breadcrumb-row text-center" style="height:130px;">
                    <h2>Chi tiết đơn hàng</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-top" style="min-height: 400px;">
        <div class="large-12 columns">
            <div class="large-8 columns side-padding">
                <form method="post">

                    <table class="table-border-bottom table-spaced-items table-center-headers">
                        <thead>
                            <tr>
                                <th class="product-name" colspan="3">Sản phẩm</th>
                                <th class="product-price hide-for-small">Giá bán</th>
                                <th class="product-quantity">Số lượng</th>
                                <th class="product-subtotal hide-for-small">Tổng tiền</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $conn = db_connect();
                            $total_price = 0;
                            $image_path = "/project-php/Images/";
                            $id = $_GET['id'];
                            $products = getProductsByOrderId($conn, $id);
                            while ($item = mysqli_fetch_assoc($products)) {
                                $itemId = $item['product_id'];
                                $product = mysqli_fetch_assoc(getProduct($conn, $itemId));
                                $total_price += $item['op_price'] * $item['quantity'];
                                echo '
                                <tr id="' . $itemId . '-row">
                                    <td class="product-remove remove-product">
                                        <span class="checkbox delete" itemId="' . $itemId . '">
                                            <input type="hidden" name="' . $itemId . '-delete" value="False" />
                                            <input class="checkbox-element" type="checkbox" name="' . $itemId . '-delete" value="True"/>
                                            <i class="fa fa-close fa-clickable" ></i>
                                        </span>
                                    </td>
                                    <td class="product-thumbnail">
                                        <a href="">
                                            <img width="50" height="50" src="' . $image_path . $product['image'] . '">
                                        </a>
                                    </td>
                                    <td class="product-name" data-title="Sản phẩm">
                                        <a href="">
                                            ' . $product['name'] . '
                                        </a>
                                    </td>
                                    <td class="product-price hide-for-small" data-title="Giá bán">
                                        <span>' . number_format($item['op_price']) . ' VNĐ </span>
                                    </td>

                                    <td class="product-quantity" data-title="Số lượng">
                                        <span>' . $item['quantity'] . '</span>
                                    </td>
                                    <td class="product-subtotal hide-for-small" data-title="Tổng tiền">
                                        <span>' . number_format($item['op_price'] * $item['quantity']) . ' VNĐ </span>
                                    </td>
                                </tr>
                                ';
                            }
                            mysqli_close($conn);
                            ?>
                        </tbody>
                    </table>
                    <a href="orders.php" class="button" style="width:100%">Quay lại danh sách đơn hàng</a>
                </form>
            </div>


            <div class="large-4 columns gray-border" style="padding:20px">
                <div class="cart_totals">
                    <h5 class="heading-title"> Tổng tiền đơn hàng </h5>

                    <table class="table-spaced-items table-right-content table-border-bottom">
                        <tbody>
                            <tr>
                                <th>Tổng tiền hàng</th>
                                <td><span><?php echo number_format($total_price) ?> VNĐ</span></td>
                            </tr>
                            <!-- <tr>
                                <th>Giao hàng</th>
                                <td><strong><span>
                                            <?php
                                            // $shippingCost = 0;
                                            // if ($total_price > 0) $shippingCost = 30000;
                                            // $total_price + $shippingCost;
                                            // echo number_format($shippingCost);
                                            ?> VNĐ
                                        </span></strong> </td>
                            </tr> -->

                            <tr class="order-total">
                                <th>Tổng tiền</th>
                                <td><strong><span><?php echo number_format($total_price) ?> VNĐ</span></strong> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
    <?php include '../layouts/footer.php'; ?>
</body>