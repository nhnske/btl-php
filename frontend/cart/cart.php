<?php
// require_once("../lib/db.php");
require_once("../lib/order_service.php");
require_once("../lib/products_service.php");
require_once("../lib/cart_helpers.php");

if (session_status() != PHP_SESSION_ACTIVE) session_start();

if (isset($_POST["update_cart"])) {
    foreach (array_keys($_SESSION['cart']) as $itemId) {
        if ($itemId == 'total') continue;
        $Quantity = $_POST["$itemId-qty"];
        $_SESSION["cart"][$itemId]["quantity"] = $Quantity;
        $Delete = $_POST["$itemId-delete"];
        if ($Delete == "True") {
            unset($_SESSION["cart"][$itemId]);
        }
    }
}

// addItem2Cart("04fb25", 1);
$cart_exists = array_key_exists('cart', $_SESSION) && !empty($_SESSION['cart']);
?>

<head>
    <?php include '../layouts/stylesheets.php'; ?>
</head>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.delete').click(function() {
                $('#' + $(this).attr("itemId") + '-row').hide();
            });
        });
    </script>

    <?php include '../layouts/header.php'; ?>

    <div id="nasa-breadcrumb-site" class="bread">
        <div class="row">
            <div class="large-12 columns nasa-display-table">
                <div class="breadcrumb-row text-center" style="height:130px;">
                    <h2>Giỏ hàng</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-top" style="min-height: 400px;">
        <div class="large-12 columns">
            <div class="large-8 columns side-padding">
                <form method="post">

                    <table class="table-border-bottom table-spaced-items table-center-headers">
                        <thead>
                            <tr>
                                <th class="product-name" colspan="3">Sản phẩm</th>
                                <th class="product-price hide-for-small">Giá bán</th>
                                <th class="product-quantity">Số lượng</th>
                                <th class="product-subtotal hide-for-small">Tổng tiền</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $conn = db_connect();
                            $total_price = 0;
                            $image_path = "/project-php/Images/";
                            if ($cart_exists) {
                                foreach (array_keys($_SESSION['cart']) as $itemId) {
                                    if ($itemId == 'total') continue;
                                    $item = $_SESSION['cart'][$itemId];
                                    $product = mysqli_fetch_assoc(getProduct($conn, $itemId));
                                    $total_price += $product['price'] * $item['quantity'];
                                    echo '
                                    <tr id="' . $itemId . '-row">
                                        <td class="product-remove remove-product">
                                            <span class="checkbox delete" itemId="' . $itemId . '">
                                                <input type="hidden" name="' . $itemId . '-delete" value="False" />
                                                <input class="checkbox-element" type="checkbox" name="' . $itemId . '-delete" value="True"/>
                                                <i class="fa fa-close fa-clickable" ></i>
                                            </span>
                                        </td>
                                        <td class="product-thumbnail">
                                            <a href="">
                                                <img width="50" height="50" src="' . $image_path . $product['image'] . '">
                                            </a>
                                        </td>
                                        <td class="product-name" data-title="Sản phẩm">
                                            <a href="">
                                                ' . $product['name'] . '
                                            </a>
                                        </td>
                                        <td class="product-price hide-for-small" data-title="Giá bán">
                                            <span>' . number_format($product['price']) . ' VNĐ </span>
                                        </td>

                                        <td class="product-quantity" data-title="Số lượng">
                                            <input type="number" min="1" max="' . $product['stock'] . '" name="' . $itemId . '-qty" value="' . $item['quantity'] . '" title="Qty">
                                        </td>
                                        <td class="product-subtotal hide-for-small" data-title="Tổng tiền">
                                            <span>' . number_format($product['price'] * $item['quantity']) . ' VNĐ </span>
                                        </td>
                                    </tr>
                                    ';
                                }
                            }
                            mysqli_close($conn);
                            ?>
                        </tbody>
                    </table>
                    <button type="submit" name="update_cart" style="width:100%;">Cập nhật giỏ hàng</button>
                    <!-- <a href="" class="button" style="width:100%">Cập nhật giỏ hàng</a> -->
                </form>
            </div>


            <div class="large-4 columns gray-border" style="padding:20px">
                <div class="cart_totals">
                    <h5 class="heading-title"> Tổng tiền đơn hàng </h5>

                    <table class="table-spaced-items table-right-content table-border-bottom">
                        <tbody>
                            <tr>
                                <th>Tổng tiền hàng</th>
                                <td><span><?php echo number_format($total_price) ?> VNĐ</span></td>
                            </tr>
                            <!-- <tr>
                                <th>Giao hàng</th>
                                <td><strong><span>
                                            <?php
                                            // $shippingCost = 0;
                                            // if ($total_price > 0) $shippingCost = 30000;
                                            // $total_price + $shippingCost;
                                            // echo number_format($shippingCost);
                                            ?> VNĐ
                                        </span></strong> </td>
                            </tr> -->

                            <tr class="order-total">
                                <th>Tổng tiền</th>
                                <td><strong><span><?php echo number_format($total_price) ?> VNĐ</span></strong> </td>
                            </tr>
                        </tbody>
                    </table>

                    <a href="/project-php/frontend/orders/orders.php" class="button" style="width:100%">Tiến hành thanh toán</a>
                </div>
            </div>


        </div>
    </div>
    <?php include '../layouts/footer.php'; ?>
</body>