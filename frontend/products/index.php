<!DOCTYPE html>
<html lang="en">

<?php
    require_once('../lib/db.php');
?>

<head>
    <?php include('../layouts/stylesheets.php');?>    
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./style/page.css">
    <title>Shop Trang Tri</title>
</head>

<body>
    <?php
    include('../layouts/header.php');
    include('./modules/content.php');
    include('../layouts/footer.php');
    ?>
</body>

</html>