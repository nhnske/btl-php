<?php
$page = 1;
if (!empty($_GET)) {
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
}
$limit = 20;
$start = ($page - 1) * $limit;
$sql_all = "select * from products order by id limit $start,$limit";
$data = db_query($conn, $sql_all);
$count = "select count(id) from products";
$data_query = db_query($conn, $count);
$total = mysqli_fetch_row($data_query);
$total_page = ceil($total[0] / 20);

?>

<p class="all">Tất cả Sản phẩm</p>
<div class="san-pham-all">
    <ul class="center-align">
        <?php
        while ($dong_all = mysqli_fetch_assoc($data)) {
            $image_path = "/project-php/Images/";
            $Image = $image_path . $dong_all['image'];
            // $Image = "../image.jpg";
            ?>
            <li class="gray-border all-padding all-margin">
                <a href="index.php?xem=chitietsanpham&id=<?php echo $dong_all['id'] ?>">
                    <img class="img_product" src="<?php echo $Image ?>">
                </a>
                <div class="product">
                    <p class="productName"><?php echo $dong_all['name'] ?></p>
                    <p class="productPrice" style="color: #F00"><?php echo number_format($dong_all['price']) ?> VNĐ</p>
                </div>
            </li>
        <?php
        }
        ?>
    </ul>

</div>
<div class="pagination">
    <a href="#">&laquo;</a>
    <?php
    for ($i = 1; $i <= $total_page; $i++) {
        ?>
        <a href="index.php?page=<?php echo $i ?>"><?php echo $i; ?></a>
    <?php
    }
    ?>

    <a href="#">&raquo;</a>
</div>