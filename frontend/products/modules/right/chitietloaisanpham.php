<?php
$page = 1;
if (!empty($_GET)) {
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
}
$limit = 20;
$start = ($page - 1) * $limit;

$sql_chitiet = "select * from products where category_id ='$_GET[id]' order by id limit $start,$limit";
$query_product = db_query($conn, $sql_chitiet);
$count = "select count(id) from products where category_id ='$_GET[id]'";
$data_query = db_query($conn, $count);
$total = mysqli_fetch_row($data_query);
$total_page = ceil($total[0] / 20);


$sql_loaisp = "select * from categories where id='$_GET[id]'";
$data = db_query($conn, $sql_loaisp);
$category = mysqli_fetch_array($data);
?>
<p class="danhmuc"><?php echo $category['name'] ?></p>

<div class="san-pham-all">
    <ul class="center-align">
        <?php
        while ($product = mysqli_fetch_assoc($query_product)) {
            $Image = "/project-php/Images/" . $product['image'];
            // $Image = "../image.jpg";
            ?>
            <li class="gray-border all-padding all-margin">
                <a href="index.php?xem=chitietsanpham&id=<?php echo $product['id'] ?>">
                    <img src="<?php echo $Image ?>" alt="" width="150">
                </a>
                <div class="product">
                    <p class="productName"><?php echo $product['name'] ?></p>
                    <p class="productprice" style="color: #F00"><?php echo number_format($product['price']) ?> VNĐ</p>
                </div>
            </li>
        <?php
        }
        ?>
    </ul>
</div>
<div class="pagination">
    <a href="#">&laquo;</a>
    <?php
    for ($i = 1; $i <= $total_page; $i++) {
        ?>
        <a href="index.php?xem=chitietloaisanpham&id=<?php echo $_GET['id'] ?>&page=<?php echo $i ?>"><?php echo $i; ?></a>
    <?php
    }
    ?>

    <a href="#">&raquo;</a>
</div>