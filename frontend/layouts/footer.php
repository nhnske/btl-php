<footer id="nasa-footer" class="footer-wrapper nasa-clear-both">
    <div data-content_placement="top" class="section-element vc_custom_1552672888847">
        <div class="row">
            <div class="large-4 nasa-col columns">
                <div class="nasa-title clearfix hr-type-none">
                    <div class="nasa-wrap">
                        <h3 class="nasa-heading-title"> Shop Trang Tri </h3>
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper">
                        <p>Chúng tôi cung cấp các mặt hàng quà tặng tân gia và đồ trang trí nhà.</p>
                        <p>Chúng tôi luôn muốn cung cấp cho bạn những sản phẩm và dịch vụ tốt nhất.</p>
                        <p>Hi vọng bạn sẽ lựa chọn được những mặt hàng ưng ý nhất.</p>
                    </div>
                </div>
            </div>
            <div class="large-4 nasa-col columns">
                <div class="nasa-title clearfix hr-type-none">
                    <div class="nasa-wrap">
                        <h3 class="nasa-heading-title">FACEBOOK </h3>
                    </div>
                </div>
                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=515882069185774&autoLogAppEvents=1"></script>
                        <div class="fb-group" data-href="https://www.facebook.com/groups/fitutc/" data-width="380" data-show-social-context="false" data-show-metadata="false"></div>
                    </div>
                </div>
            </div>
            <div class="large-4 nasa-col columns">
                <div class="nasa-title clearfix hr-type-none">
                    <h3>THÔNG TIN LIÊN HỆ</h3>
                </div>
                <ul class="contact-information">
                    <li class="media">Số 3 Đường Cầu Giấy, Láng Thượng, Đống Đa, Hà Nội</li>
                    <li class="media">0987 654 321</li>
                    <li class="media">shoptrangtri@gmail.com </li>
                    <li class="media">www.shoptrangtri.com </li>
                </ul>
                <ul class="nasa-opening-time">
                    <li><span>Thứ hai - Thứ sáu</span><span>07:00 - 17:15 </span></li>
                    <li><span>Thứ bảy</span><span>13:00 - 17:15 </span></li>
                    <li><span>Chủ nhật</span><span>- Đóng cửa - </span></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>

</html>