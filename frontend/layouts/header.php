<?php
if (session_status() != PHP_SESSION_ACTIVE) session_start();
if (isset($_SESSION['admin'])) {
    header("location: /project-php/backend/");
};
require_once("../lib/db.php");
require_once("../lib/cart_helpers.php");
?>
<div id="top-bar"></div>
<header id="masthead" class="site-header">
    <div class="row">
        <div class="large-12 columns">
            <div class="row ">
                <div class="large-4 columns">
                    <div class="social-icons nasa-follow">
                        <div class="follow-icon">
                            <a href="https://www.facebook.com/">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://www.gmail.com/">
                                <i class="fa fa-envelope-o"></i>
                            </a>
                            <a href="https://www.youtube.com/">
                                <i class="fa fa-youtube-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="large-4 columns text-center">
                    <div class="logo-wrapper nasa-fullwidth">
                        <h1 class="nasa-logo-img">
                            <a class="logo nasa-logo-retina" href="/project-php/" title="Shop Trang Tri">
                                <img src="/project-php/frontend/layouts/content/logo.jpg" class="header_logo">
                            </a>
                        </h1>
                    </div>
                </div>
                <div class="large-4 columns">
                    <div class="nasa-header-icons-wrap">
                        <ul class="header-icons">
                            <li class="nasa-icon-mini-cart">
                                <div class="mini-cart cart-inner mini-cart-type-full inline-block">
                                    <a href="/project-php/frontend/cart/cart.php" class="cart-link" title="Giỏ hàng">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span class="products-number nasa-product-empty">
                                            <span class="nasa-sl"><?php echo getCartCount(); ?></span>
                                            <span class="hidden-tag nasa-sl-label last">Items</span>
                                        </span>
                                    </a>
                                </div>
                            </li>
                            <?php
                                if (!isset($_SESSION['customer'])) {
                                    echo '
                                    <li class="nasa-icon-sign-in">
                                        <a href="/project-php/backend/" data-open="0" title="Đăng nhập">
                                            <i class="fa fa-sign-in"></i>
                                        </a>
                                    </li>
                                    ';
                                }  else {
                                    echo '
                                    <li class="nasa-icon-sign-out">
                                        <a href="/project-php/frontend/user/sign-out.php" data-open="0" title="Đăng xuất">
                                            <i class="fa fa-sign-out"></i>
                                        </a>
                                    </li>
                                    ';
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="header-bar">
        <div class="row nav-wrapper inline-block main-menu-warpper">
            <ul id="site-navigation" class="header-nav">
                <li class="menu-item menu-parent-item default-menu root-item">
                    <a title="SẢN PHẨM" href="/project-php/frontend/products/">
                        <i class="fa fa-angle-down"></i>
                        <span class="nasa-text-menu">SẢN PHẨM</span>
                    </a>
                    <div class="nav-dropdown">
                        <ul class="sub-menu">
                            <?php
                            $conn = db_connect();
                            $sql_loaisp = "select * from categories";
                            $data = db_query($conn, $sql_loaisp);
                            while ($category = mysqli_fetch_assoc($data)) {
                                echo '
                                    <li class="menu-item">
                                        <a title="' . $category['name'] . '" href="/project-php/frontend/products/index.php?xem=chitietloaisanpham&id=' . $category['id'] . '">
                                            <span class="nasa-text-menu">' . $category['name'] . '</span>
                                        </a>
                                    </li>
                                    ';
                            }
                            db_close($conn);
                            ?>
                        </ul>
                    </div>
                </li>
                <li class="menu-item root-item">
                    <a title="GIỚI THIỆU" href="">
                        <span class="nasa-text-menu">GIỚI THIỆU</span>
                    </a>
                </li>
                <li class="menu-item root-item">
                    <a href="">
                        <span class="nasa-text-menu">LIÊN HỆ</span>
                    </a>
                </li>
                <li class="menu-item menu-parent-item default-menu root-item">
                    <a title="NGƯỜI DÙNG" href="/project-php/frontend/users/">
                        <i class="fa fa-angle-down"></i>
                        <span class="nasa-text-menu">NGƯỜI DÙNG</span>
                    </a>
                    <div class="nav-dropdown">
                        <ul class="sub-menu">
                            <?php
                                if (!isset($_SESSION['customer'])) {
                                    echo '
                                    <li class="menu-item">
                                        <a title="sign-in" href="/project-php/backend/index.php?controller=admin&action=login">
                                            <span class="nasa-text-menu">Đăng nhập</span>
                                        </a>
                                    </li>
                                    <li class="menu-item">
                                        <a title="sign-up" href="/project-php/frontend/user/sign-up.php">
                                            <span class="nasa-text-menu">Đăng kí</span>
                                        </a>
                                    </li>
                                    ';
                                }  else {
                                    echo '
                                    <li class="menu-item">
                                        <a title="profile" href="/project-php/frontend/user/profiles.php">
                                            <span class="nasa-text-menu">Thông tin tài khoản</span>
                                        </a>
                                    </li>
                                    <li class="menu-item">
                                        <a title="signout" href="/project-php/frontend/user/sign-out.php">
                                            <span class="nasa-text-menu">Đăng xuất</span>
                                        </a>
                                    </li>
                                    <li class="menu-item">
                                        <a title="orders" href="/project-php/frontend/user/orders.php">
                                            <span class="nasa-text-menu">Đơn hàng</span>
                                        </a>
                                    </li>
                                    ';
                                }
                            ?>
                            <li class="menu-item">
                                <a title="cart" href="/project-php/frontend/cart/cart.php">
                                    <span class="nasa-text-menu">Giỏ hàng</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>