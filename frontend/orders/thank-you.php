<?php
$timeout = 5;
header("Refresh:5; url=/project-php/");
?>

<head>
    <link rel="stylesheet" href="/project-php/frontend/layouts/content/bootstrap.min.css">
    <style>
        .container {
            width: 70%;
            position: absolute;
            top: 50%;
            left: 50%;
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }
    </style>
</head>

<body>
    <div class="jumbotron text-center container">
        <h1 class="display-3">Cảm ơn bạn đã mua hàng!</h1>
        <p class="lead">Chúng tôi sẽ sớm liên lạc với bạn để hoàn tất đơn hàng</p>
        <hr>
        <p id="timer">
            <?php echo "Bạn sẽ được chuyển hướng sau $timeout giây."; ?>
        </p>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="/project-php/frontend/products/" role="button">Quay lại trang chủ</a>
        </p>
    </div>
</body>

<?php
echo '
<script>
    var seconds = ' . $timeout . ';
    var x = setInterval(function() {
        seconds -= 1;
        document.getElementById("timer").innerHTML = "Bạn sẽ được chuyển hướng sau " + seconds + " giây.";
    }, 1000);
</script>
';
?>