<?php
require("../lib/db.php");
require("../lib/order_service.php");
require("../lib/customer_service.php");
require("../lib/products_service.php");
require_once("../lib/cart_helpers.php");

if (session_status() != PHP_SESSION_ACTIVE) session_start();
$cart_exists = array_key_exists('cart', $_SESSION) && !empty($_SESSION['cart']);


if (isset($_POST["place_order"]) && $cart_exists) {
    if (!isset($_SESSION['customer'])) {
        $conn = db_connect();

        $Name = $_POST["billing_first_name"];
        $Address = $_POST["billing_address"];
        $Phone = $_POST["billing_phone"];
        $Email = $_POST["billing_email"];
        $CustomerId = createCustomer($conn, $Name, $Address, $Phone, $Email);

        $Products = $_SESSION['cart'];
        $Note = $_POST["order_comments"];

        $OrderId = createOrder($conn, $CustomerId, $Products, $Note);
        mysqli_close($conn);
        emptyCart();
        header('Location: /project-php/frontend/orders/thank-you.php');
    } else {
        $conn = db_connect();
        $CustomerId = $_SESSION['customer']['customer_id'];
        $Products = $_SESSION['cart'];
        $Note = $_POST["order_comments"];
        $OrderId = createOrder($conn, $CustomerId, $Products, $Note);
        mysqli_close($conn);
        emptyCart();
        header('Location: /project-php/frontend/orders/thank-you.php');
    }
}
?>

<head>
    <?php include '../layouts/stylesheets.php'; ?>
</head>

<body>

    <?php include '../layouts/header.php'; ?>

    <div id="nasa-breadcrumb-site" class="bread">
        <div class="row">
            <div class="large-12 columns nasa-display-table">
                <div class="breadcrumb-row text-center" style="height:130px;">
                    <h2>Trang đặt hàng</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="order-steps">
        <div class="row">
            <div class="large-12 columns">
                <div class="checkout-breadcrumb">
                    <div class="title-cart">
                        <a href="/project-php/frontend/cart/cart.php" title="Giỏ hàng">
                            <h1>01</h1>
                            <h4>Giỏ hàng</h4>
                            <p>Quản lý giỏ hàng</p>
                        </a>
                    </div>

                    <div class="title-checkout">
                        <a href="/project-php/frontend/orders/orders.php" title="Chi tiết đơn hàng">
                            <h1>02</h1>
                            <h4>Chi tiết đơn hàng</h4>
                            <p>Kiểm tra số lượng sản phẩm</p>
                        </a>
                    </div>

                    <div class="title-thankyou">
                        <h1>03</h1>
                        <h4>Hoàn tất đơn hàng</h4>
                        <p>Xác nhận đặt hàng</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <form method="post">
            <div class="large-6 columns checkout-group">
                <h3>Thông tin thanh toán</h3>
                <?php
                if (!isset($_SESSION['customer'])) {
                    echo '
                    <label for="billing_first_name">Tên của bạn</label>
                    <input type="text" name="billing_first_name" id="billing_first_name" required>
                    <label for="billing_address_1">Địa chỉ giao hàng</label>
                    <input type="text" name="billing_address" id="billing_address" placeholder="Số nhà và tên đường" required>
                    <label for="billing_phone">Số điện thoại</label>
                    <input type="tel" name="billing_phone" id="billing_phone" pattern="^(?:0|\(?\+84\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" required>
                    <label for="billing_email">Địa chỉ email (tuỳ chọn)</label>
                    <input type="email" name="billing_email" id="billing_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                    ';
                }
                ?>
                <label for="order_comments">Ghi chú đơn hàng (tuỳ chọn)</label>
                <textarea name="order_comments" class="input-text " id="order_comments" placeholder="Ghi chú về đơn hàng, ví dụ: lưu ý khi giao hàng." rows="2" cols="5"></textarea>
            </div>

            <div class="large-6 columns checkout-group">
                <h3>TỔNG TIỀN ĐƠN HÀNG</h3>
                <div>
                    <table class="table-spaced-items">
                        <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Giá</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $conn = db_connect();
                            $total_price = 0;
                            if ($cart_exists) {
                                foreach (array_keys($_SESSION['cart']) as $itemId) {
                                    $item = $_SESSION['cart'][$itemId];
                                    $product = mysqli_fetch_assoc(getProduct($conn, $itemId));
                                    $_SESSION['cart'][$itemId]['price'] = $product['price'];
                                    $total_price += $product['price'] * $item['quantity'];
                                    echo "<tr>";
                                    echo "<td>" . $product['name'] . " × " . $item['quantity'] . "</td>";
                                    echo "<td>" . number_format($product['price'] * $item['quantity']) . " VNĐ</td>";
                                    echo "</tr>";
                                }
                            }
                            mysqli_close($conn);
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tổng</th>
                                <td><strong><?php echo number_format($total_price) ?> VNĐ</span></strong> </td>
                            </tr>
                        </tfoot>
                    </table>

                    <div>
                        <!-- <ul>
                            <li>
                                <input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" checked="checked">
                                <span>Chuyển khoản ngân hàng</span>
                            </li>
                            <li>
                                <input id="payment_method_cod" type="radio" class="input-radio" name="payment_method" value="cod">
                                <span>Trả tiền mặt khi nhận hàng</span>
                            </li>
                        </ul> -->
                        <button type="submit" class="button alt" name="place_order" id="place_order" value="Đặt hàng">Đặt hàng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?php include '../layouts/footer.php'; ?>
</body>