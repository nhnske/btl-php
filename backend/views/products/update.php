<?php include_once 'views/layout/header.php' ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <h2>Cập nhật sản phẩm #<?php echo $product['id']?></h2>
        <form method="POST" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label>Tên sản phẩm</label>
                <input type="text" name="name"
                       value="<?php echo isset($_POST['name']) ? $_POST['name'] : $product['name']; ?>"
                       class="form-control"/>
            </div>
            <div class="form-group">
                <label>Danh mục sản phẩm</label>
                <select class="form-control" name="category_id">
                  <?php if (!empty($categories)):
                    $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : $product['category_id'];
                    ?>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?php echo $category['id'] ?>" <?php echo $category['id'] == $category_id ? "selected=true" : null ?>>
                        <?php echo $category['name'] ?>
                      </option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    Upload ảnh đại diện
                    (File dạng ảnh, dung lượng upload không vượt quá 2Mb)
                </label>
                <input type="file" name="image" id="imageInput" class="form-control" >
                <?php if (!empty($product['image'])): ?>
                    <img id="image" src="/project-php/Images/<?php echo $product['image']?>" width="60" />
                <?php endif; ?>
            </div>
            
            <div class="form-group">
                <label>Số lượng</label>
                <input type="number" name="stock"
                       value="<?php echo isset($_POST['stock']) ? $_POST['stock'] : $product['stock']; ?>"
                       class="form-control"/>
            </div>
            <div class="form-group">
                <label>Giá</label>
                <input type="number" name="price"
                       value="<?php echo isset($_POST['price']) ? $_POST['price'] : $product['price']; ?>"
                       class="form-control"/>
            </div>
            <div class="form-group">
              
                <label>Status</label>
                <select name="status" class="form-control">
                    <option <?php echo $product['status'] == 1 ?'selected':""; ?> value="1">
                        Enabled
                    </option>
                    <option <?php echo $product['status'] == 0 ?'selected':""; ?> value="0">
                        Disabled
                    </option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" name="submit"
                       class="btn btn-success" value="Save"/>
                <a href="index.php?controller=product&action=index"
                   class="btn btn-secondary">Cancel</a>
            </div>
        </form>
</div>
</section>


<!-- /.content -->
</div>


<?php include_once 'views/layout/footer.php' ?>
