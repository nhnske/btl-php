<?php include_once 'views/layout/header.php' ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <section class="content">
  <div class="search-form">
            <h4>Tìm kiếm</h4>
            <form action="index.php?controller=product&action=index" method="GET">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <label>Tên Sản phẩm</label>
                        <input type="text" name="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''?>" class="form-control"/>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label>Danh mục sản phẩm</label>
                        <select class="form-control" name="category_id">
                          <?php if (!empty($categories)): ?>
                          <option value="0">Select</option>
                            <?php foreach ($categories as $category): ?>
                                  <option value="<?php echo $category['id'] ?>" <?php echo isset($_GET['category_id']) && $category['id'] == $_GET['category_id'] ? "selected=true" : null ?>>
                                    <?php echo $category['name'] ?>
                                  </option>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </select>
                    </div>
                    
                    <input type="hidden" name="controller" value="<?php echo isset($_GET['controller']) ? $_GET['controller'] : ''?>"/>
                    <input type="hidden" name="action" value="<?php echo isset($_GET['action']) ? $_GET['action'] : ''?>"/>
                </div>
                <br />
                <div class="form-group">
                    <button type="submit" name="submit_search" class="btn btn-success">
                        <span class="fa fa-search"></span> Tìm kiếm
                    </button>
                    <a href="index.php?controller=product&action=index" class="btn btn-secondary">Cancel</a>
                </div>
            </form>
        </div>


    <a class="btn btn-primary" href="index.php?controller=product&action=create">
      <span class="glyphicon glyphicon-plus"></span>
      Thêm mới
    </a>

    <h2>Danh sách sản phẩm</h2>
    <table class="table table-bordered">
      <tr>
        <th>ID</th>
        <th>Danh mục</th>
        <th>Tên sản phẩm</th>
        <th>Giá</th>
        <th>Hình ảnh</th>
        <th>Trạng thái</th>
        <th>Hành động</th>
      </tr>
      <?php if (!empty($products)) : ?>
        <?php foreach ($products as $product) : ?>
          <tr>
            <td>
              <?php echo $product['id']; ?>
            </td>
            <td>
              <?php echo $product['category_name']; ?>
            </td>
            <td>
              <?php echo $product['name']; ?>
            </td>
            <td>
              <?php echo number_format($product['price']); ?>VNĐ
            </td>
            <td>
              <?php if (!empty($product['image'])) : ?>
                <img src="/project-php/Images/<?php echo $product['image'] ?>" width="80px" />
              <?php endif; ?>
            </td>
            <td>
              <?php echo $product['status'] == '1' ?   "Actived" : "Disabled"; ?>
            </td>

            <td>
              <?php
                  $urlUpdate = 'index.php?controller=product&action=update&id=' . $product['id'];
                  $urlDelete = 'index.php?controller=product&action=delete&id=' . $product['id'];
                  ?>

              <a href="<?php echo $urlUpdate ?>">
                <span class="fa fa-pencil"></span>
              </a> &nbsp;
              <a href="<?php echo $urlDelete ?>" onclick="return confirm('Bạn có chắc chắn muốn xóa bản ghi này hay không?');">
                <span class="fa fa-trash"></span>
              </a> &nbsp;
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="7">
            Không có bản ghi nào
          </td>
        </tr>
      <?php endif; ?>
    </table>
    <?php
    //hiển thị phân trang
    echo $pages;
    ?>
  </section>

</div>

<?php include_once 'views/layout/footer.php' ?>