<?php include_once 'views/layout/header.php' ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">



    

    <h2>Danh sách Khách hàng</h2>
    <table class="table table-bordered">
      <tr>
        <th>Số thứ tự</th>
        <th>Tên tài khoản</th>
        <th>Họ tên</th>
        <th>Địa chỉ</th>
        <th>Số điện thoại</th>
        <th>Email</th>
        <th>Hành động</th>
      </tr>
      <?php if (!empty($users)) : ?>
        <?php $i = 1; ?>
        <?php foreach ($users as $user) : ?>
          <tr>
            <td>
              <?php echo $i++; ?>
            </td>
            <td>
              <?php echo $user['username']; ?>
            </td>
            <td>
              <?php echo $user['name']; ?>
            </td>
            <td>
              <?php echo $user['address']; ?>
            </td>
            <td>
              <?php echo $user['phone']; ?>
            </td>
            <td>
              <?php echo $user['email']; ?>
            </td>

            <td>
              <?php
                  $urlUpdate = 'index.php?controller=product&action=update&id=' . $user['id'];
                  $urlDelete = 'index.php?controller=product&action=delete&id=' . $user['id'];
                  ?>

              <a href="<?php echo $urlUpdate ?>">
                <span class="fa fa-pencil"></span>
              </a> &nbsp;
              <a href="<?php echo $urlDelete ?>" onclick="return confirm('Bạn có chắc chắn muốn xóa bản ghi này hay không?');">
                <span class="fa fa-trash"></span>
              </a> &nbsp;
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="7">
            Không có bản ghi nào
          </td>
        </tr>
      <?php endif; ?>
    </table>

  </section>

</div>

<?php include_once 'views/layout/footer.php' ?>