<?php include_once 'views/layout/header.php' ?>

<div class="content-wrapper">



  <section class="content">
    <h2>Danh sách đơn hàng</h2>
    <table class="table table-bordered">
      <tr>
        <th>Mã đơn</th>
        <th>Họ tên</th>
        <th>Địa chỉ</th>
        <th>SĐT</th>
        <th>Email</th>
        <th>Ghi chú</th>
        <th>Tổng tiền</th>
        <th>Trạng thái thanh toán</th>
        <th>Ngày tạo đơn</th>
        <th>Action</th>
      </tr>
      <?php if (!empty($orders)) : ?>
        <?php foreach ($orders as $order) :
            $id = $order['order_id'];
            ?>
          <tr>
            <td>
              <?php echo $order['order_id']; ?>
            </td>
            <td>
              <?php echo $order['customer_name']; ?>
            </td>
            <td>
              <?php echo $order['customer_address']; ?>
            </td>
            <td>
              <?php echo $order['customer_phone']; ?>
            </td>
            <td>
              <?php echo $order['customer_email']; ?>
            </td>
            <td>
              <?php echo $order['order_note']; ?>
            </td>
            <td>
              <?php echo number_format($order['sum_price'], 0, '.', '.'); ?> VNĐ
            </td>
            <td>
              <?php
                  $unpaid = "<a href='index.php?controller=order&action=updatePaymentStatus&id=$id' class='payment-status' onclick='return confirm(\"Chắc chắn bạn muốn cập nhật trạng thái thành Đã thanh toán?\")'>Xác nhận thanh toán</a>";

                  echo   $order['order_status'] == '1' ?   "Đã thanh toán" : "Chưa thanh toán $unpaid";
                  ?>



            </td>
            <td>
              <?php
                  echo date('d-m-Y H:i:s', strtotime($order['order_created_at']));
                  ?>
            </td>
            <td>
              <a href="index.php?controller=order&action=orderDetail&order_id=<?php echo $id; ?>" title="Chi tiết đơn hàng #<?php echo $id; ?>">
                <span class="fa fa-eye"></span>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

      <?php else : ?>
        <tr>
          <td colspan="7">
            Không có bản ghi nào
          </td>
        </tr>
      <?php endif; ?>
    </table>
    <?php
    //hiển thị phân trang
    echo $pages;
    ?>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once 'views/layout/footer.php' ?>