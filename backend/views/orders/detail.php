<?php include_once 'views/layout/header.php' ?>
<div class="content-wrapper">

    <section class="content">
        <h2>Danh sách đơn hàng</h2>
        <table class="table table-bordered">
            <tr>
                <th>Mã đơn</th>
                <th>Tên sản phẩm</th>
                <th>Số lượng đặt</th>
                <th>Giá</th>
                <th>Thành tiền</th>
            </tr>

            <?php if (!empty($orders)) : ?>
                <?php $sum =0; ?>
                <?php foreach ($orders as $order) :
                        ?>
                        
                    <tr>
                        <td>
                            <?php echo $order['order_id']; ?>
                        </td>
                        <td>
                            <?php echo $order['product_name']; ?>
                        </td>
                        <td>
                            <?php echo $order['quantity']; ?>
                        </td>
                        <td>
                            <?php echo number_format($order['product_price'], 0, '.', '.'); ?> VNĐ
                        </td>
                        <td>
                            <?php
                                    $totalPrice = $order['quantity'] * $order['product_price'];
                                    $sum += $totalPrice;
                                    echo number_format($totalPrice, 0, '.', '.'); ?> VNĐ
                        </td>

                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th colspan="4">Tổng tiền</th>
                    <td>
                        <?= number_format($sum, 0, '.', '.');  ?>VNĐ 
                    </td>
                </tr>
            <?php else : ?>
                <tr>
                    <td colspan="5">
                        Không có bản ghi nào
                    </td>
                </tr>
            <?php endif; ?>
        </table>

        <a href="index.php?controller=order&action=index" class="btn btn-secondary">Back</a>
    </section>
</div>
<?php include_once 'views/layout/footer.php' ?>