<?php

require_once 'controllers/Controller.php';
require_once 'models/User.php';
class UserController extends Controller
{
    public $pageTitle = "Khách hàng";

    public function index()
    {
        $userModel = new User();
        $users = $userModel->getAllUser();

        require_once 'views/users/index.php';
    }



    public function create()
    {
        $categoryModel = new Category();
        $categories = $categoryModel->getAllCategory();

        if (isset($_POST['submit'])) {
            $name = isset($_POST['name']) ? $_POST['name'] : Null;
            $price = isset($_POST['price']) ? $_POST['price'] : Null;
            $status = isset($_POST['status']) ? $_POST['status'] : Null;
            $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : Null;
            $stock = isset($_POST['stock']) ? $_POST['stock'] : Null;
            $imageArr = $_FILES['image']['size'] > 0  ? $_FILES['image'] : Null;



            $image = '';
            if ($imageArr['error'] == 0) {
                $absolutePathUpload = __DIR__ . '/../../Images';
                if (!file_exists($absolutePathUpload)) {
                    mkdir($absolutePathUpload);
                }

                $fileName = time() . $imageArr['name'];
                $isUpload = move_uploaded_file(
                    $imageArr['tmp_name'],
                    $absolutePathUpload . '/' . $fileName
                );
                if ($isUpload) {
                    $image = $fileName;
                }
            }


            $product =
                [
                    'id' => $this->generateId(),
                    'name' => $name,
                    'category_id' => $category_id,
                    'price' => $price,
                    'status' => $status,
                    'stock' => $stock,
                    'image' => $image
                ];
            $productModel = new Product();
            $isInsert = $productModel->insert($product);
            if ($isInsert) {
                $_SESSION['success'] = 'Insert thành công';
            } else {
                $_SESSION['error'] = 'Insert Thất bại';
            }
            header('Location: index.php?controller=product&action=index');
            exit();
        }

        require_once 'views/products/create.php';
    }

    public function update()
    {
        if (!isset($_GET['id'])) {
            $_SESSION['error'] = 'Tham số ID không hợp lệ';
            header('Location: index.php?controller=product&action=index');
            exit();
        }
        $categoryModel = new Category();
        $categories = $categoryModel->getAllCategory();

        $productModel = new Product();
        $product = $productModel->getById($_GET['id']);


        if (isset($_POST['submit'])) {
            $name = isset($_POST['name']) ? $_POST['name'] : $product['name'];
            $price = isset($_POST['price']) ? $_POST['price'] : $product['price'];
            $status = isset($_POST['status']) ? $_POST['status'] : $product['status'];
            $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : $product['category_id'];
            $stock = isset($_POST['stock']) ? $_POST['stock'] : $product['stock'];
            $imageArr = $_FILES['image']['size'] > 0  ? $_FILES['image'] : Null;



            $image = $product['image'];
            if ($imageArr['error'] == 0) {

                $absolutePathUpload = __DIR__ . '/../../Images';

                if (!empty($avatar)) {
                    @unlink($absolutePathUpload . '/' . $image);
                }
                if (!file_exists($absolutePathUpload)) {
                    mkdir($absolutePathUpload);
                }

                $fileName = time() . $imageArr['name'];
                $isUpload = move_uploaded_file(
                    $imageArr['tmp_name'],
                    $absolutePathUpload . '/' . $fileName
                );
                if ($isUpload) {
                    $image = $fileName;
                }
            }


            $productUpdate =
                [
                    'id' => $product['id'],
                    'name' => $name,
                    'category_id' => $category_id,
                    'price' => $price,
                    'status' => $status,
                    'stock' => $stock,
                    'image' => $image
                ];


            $isUpdate = $productModel->update($productUpdate);
            if ($isUpdate) {
                $_SESSION['success'] = 'Update thành công';
            } else {
                $_SESSION['error'] = 'Update thất bại';
            }
            header("Location: index.php?controller=product&action=index");
            exit();
        }



        require_once 'views/products/update.php';
    }

    public function delete()
    {

        if (!isset($_GET['id'])) {
            $_SESSION['error'] = 'Tham số ID không hợp lệ';
            header("Location: index.php?controller=product&action=index");
            exit();
        }
        $id = $_GET['id'];
        $productModel = new Product();
        $isDelete = $productModel->delete($id);
        if ($isDelete) {
            $_SESSION['success'] = "Xóa bản ghi #$id thành công";
        } else {
            $_SESSION['error'] = "Xóa bản ghi #$id thất bại";
        }

        header("Location: index.php?controller=product&action=index");
        exit();
    }
}
