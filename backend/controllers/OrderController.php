<?php
require_once 'controllers/Controller.php';
require_once 'models/Order.php';
class OrderController extends Controller
{
    public $pageTitle = "Đơn hàng";

    public function index()
    {
        $orderModel = new Order();
        $orders = $orderModel->getAllOrder();
        $pages = $orderModel->getPagination('orders');
        require_once 'views/orders/index.php';
    }

    public function updatePaymentStatus()
    {
        if (!isset($_GET['id'])) {
            $_SESSION['error'] = 'Tham số id không hợp lệ';
            header("Location: index.php?controller=Order&action=index");
            exit();
        }

        $id = $_GET['id'];
        $orderModel = new Order();

        $isUpdate = $orderModel->updatePaymentStatus($id);
        if ($isUpdate) {


            $_SESSION['success'] = "Cập nhật trạng thái thanh toán cho id #$id thành công";
        } else {
            $_SESSION['error'] = "Cập nhật trạng thái thanh toán thất bại";
        }
        header("Location: index.php?controller=order");
        exit();
    }

    public function orderDetail()
    {
        if (!isset($_GET['order_id'])) {
            $_SESSION['error'] = 'Tham số order id không hợp lệ';
            header("Location: index.php?controller=order");
            exit();
        }

        $order_id = $_GET['order_id'];
       
        $orderModel = new Order();
        $orders = $orderModel->getDetailOrder($order_id);
    
        require_once 'views/orders/detail.php';
    }
}
