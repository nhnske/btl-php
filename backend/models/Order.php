<?php
require_once 'models/Model.php';

class Order extends Model
{ 
    public function getAllOrder(){
       
        $conn = $this->db_connect();
        $querySelect = "SELECT orders.id as order_id,customers.`name` as customer_name, customers.address  as customer_address, customers.phone as customer_phone,  customers.email as customer_email, orders.created_at as order_created_at,SUM(orders_products.price*orders_products.quantity) as sum_price,orders.note as order_note,orders.status as order_status
        from  orders
        LEFT JOIN customers
        on orders.customer_id = customers.id
         left JOIN orders_products 
        on orders.id = orders_products.order_id
        LEFT JOIN products
        on products.id = orders_products.product_id
        GROUP BY order_id
        LIMIT {$this->start}, {$this->limit}
        ";
        $results = mysqli_query($conn, $querySelect);
        $orders = [];
        if (mysqli_num_rows($results) > 0) {
            $orders = mysqli_fetch_all($results, MYSQLI_ASSOC);
        }
        $this->db_close($conn);
        return $orders;
    }


    public function updatePaymentStatus($id){
        $conn = $this->db_connect();
        $id = $this->escapeParam($conn,$id);
        $queryUpdate = "
        UPDATE orders SET `status` = " . 1 . "
        WHERE id = $id
        ";
        $isUpdate = mysqli_query($conn, $queryUpdate);
        return $isUpdate;
    }

    public function getDetailOrder($order_id)
    {
        $conn = $this->db_connect();
        $order_id = $this->escapeParam($conn,$order_id);
        $querySelect = "SELECT orders_products.*, products.name as product_name, orders_products.price as product_price
        FROM orders
        INNER JOIN orders_products ON orders_products.order_id = orders.id
        INNER JOIN products ON products.id = orders_products.product_id
        WHERE orders.id = '{$order_id}'
                        ";
        $results = mysqli_query($conn, $querySelect);
    
        $orders = [];
        if (mysqli_num_rows($results) > 0) {
          $orders = mysqli_fetch_all($results, MYSQLI_ASSOC);
        }
        $this->db_close($conn);
    
        return $orders;
    }

}
