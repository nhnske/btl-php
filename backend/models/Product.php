<?php

require_once 'models/Model.php';
class Product extends Model
{
    function getAllProduct()
    {
        $conn = $this->db_connect();
        $querySearch = $this->getQuerySearch();

        $querySelect = "SELECT products.*, categories.name as category_name from products inner join categories on categories.id = products.category_id 
        {$querySearch}
        ORDER BY products.created_at DESC
            LIMIT {$this->start}, {$this->limit}
        ";

        $results = mysqli_query($conn, $querySelect);
        $products = [];
        if (mysqli_num_rows($results) > 0) {
            $products = mysqli_fetch_all($results, MYSQLI_ASSOC);
        }
        $this->db_close($conn);
        return $products;
    }

    function insert($product = [])
    {
        $conn = $this->db_connect();
        foreach ($product as $key => $value) {
            $product[$key] = $this->escapeParam($conn, $value);
        }

        $queryInsert = "INSERT into products
        (`id`,`name`,`category_id`,`price`,`stock`,`status`,`image`)
        values ( '{$product['id']}',
        '{$product['name']}',
        '{$product['category_id']}',
        {$product['price']},
        {$product['stock']},
        {$product['status']},
        '{$product['image']}')";


        $isInsert = mysqli_query($conn, $queryInsert);
        $this->db_close($conn);

        return $isInsert;
    }

    public function  getById($id)
    {
        $conn = $this->db_connect();
        $id = $this->escapeParam($conn, $id);
        $querySelect = "
            SELECT products.*,categories.`name` as category_name
            from products
            left JOIN categories on products.category_id = categories.id
            where products.id = '{$id}'
        ";
        $results = mysqli_query($conn, $querySelect);
        $product = [];
        if (mysqli_num_rows($results) == 1) {
            $products = mysqli_fetch_all(
                $results,
                MYSQLI_ASSOC
            );
            $product = $products[0];
        }

        return $product;
    }

    public function update($product = [])
    {
        $conn = $this->db_connect();
        foreach ($product as $key => $value) {
            $product[$key] = $this->escapeParam($conn, $value);
        }
        $queryUpdate  = "UPDATE products
            set
            `category_id` = '{$product["category_id"]}',
            `name` = '{$product["name"]}',
            `price` = {$product["price"]},
            `stock` = {$product["stock"]},
            `status` = {$product["status"]},
            `image` = '{$product["image"]}'
        WHERE `id` = '{$product['id']}' ";
        $isUpdate = mysqli_query($conn, $queryUpdate);
        $this->db_close($conn);
        return $isUpdate;
    }

    public function delete($id ){
        $conn = $this->db_connect();
        $id = $this->escapeParam($conn,$id);
        $queryDelete = "DELETE FROM products WHERE id='$id'";
        $isDelete = mysqli_query($conn, $queryDelete);
        $this->db_close($conn);
        return $isDelete;
    }
}
