<?php
require_once 'configs/conf.php';

class Model
{
    // phân trang
    public $page;
    public $limit = 10;
    public $start;
    // param search
    // public $querySearch;
    public function __construct()
    {
        // kiểm tra xem trong config có limit, total đủ điều kiện không
        $this->page = (int) !isset($_GET['page']) ? 1 : $_GET['page'];
        if ($this->page < 0) $this->page = 1;
        $this->start = ($this->page * $this->limit) - $this->limit;
    }

    function db_connect()
    {
        global $conf;
        $conn = mysqli_connect($conf["host"], $conf["user"], $conf["password"], $conf["db"]) or die("Cannot connect to db: " . mysqli_connect_error());
        mysqli_set_charset($conn, "utf8");

        return $conn;
    }

    function db_close($conn)
    {
        mysqli_close($conn);
    }

    function db_query($conn, $query)
    {
        $result = mysqli_query($conn, $query);
        if (!$result) {
            die("Error execute query: " . mysqli_error($conn));
        }

        return $result;
    }

    function escapeParam($conn, $key)
    {
        return mysqli_real_escape_string($conn, $key);
    }

    // phân trang
    function getPagination($table)
    {
        //lấy địa chỉ trang hiện tại
        $url = $_SERVER['REQUEST_URI'];

        //ktra query string
        if (!empty($_SERVER['QUERY_STRING'])) {
            $url .= '&';
        }
        $conn = $this->db_connect();

        // tổng bản ghi phân trang
        $querySelect = "SELECT COUNT(*) as `num` FROM {$table}
         ";
        $results = mysqli_query($conn, $querySelect);
        $rowArr = mysqli_fetch_all($results, MYSQLI_ASSOC);
        $total = $rowArr[0]['num'];

        // xủ lý hiển thị phân trang
        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $lastlabel = "Last &rsaquo;&rsaquo;";

        $this->page = ($this->page == 0 ? 1 : $this->page);
        $start = ($this->page - 1) * $this->limit;
        $prev = $this->page - 1;
        $next = $this->page + 1;
        $total_page = ceil($total / $this->limit);
        $pagination = "";
        if ($total_page > 1) {
            $pagination .= "<ul class= 'pagination'>";
            $pagination .= "<li class='page_info'>Page {$this->page} of {$total_page}</li>";
            if ($this->page > 1) $pagination .= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";
            if ($total_page < 4) {
                $pagination .= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination .= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination .= "<li class='dot'>..</li>";
                for ($i = 1; $i <= $total_page; $i++) {
                    if ($i == $this->page)
                        $pagination .= "<li><a class='current'>{$i}</a></li>";
                    else
                        $pagination .= "<li><a href='{$url}page={$i}'>{$i}</a></li>";
                }
            }
            if ($this->page < $total_page - 1) {
                $pagination .= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
                $pagination .= "<li><a href='{$url}page=$total_page'>{$lastlabel}</a></li>";
            }


            $pagination .= "</ul>";
        }
        return $pagination;
    }

    public function getQuerySearch()
    {
        
        $querySearch = ' WHERE TRUE';
        if (isset($_GET['name']) && !empty($_GET['name'])) {
           
            $querySearch .= " AND products.name LIKE '%{$_GET['name']}%'";
        }
        if (isset($_GET['category_id']) && !empty($_GET['category_id'])) {
            $querySearch .= " AND products.category_id = '{$_GET['category_id']}'";
        }
        
        return  $querySearch;
    }
}
