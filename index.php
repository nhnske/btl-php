<?php

    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    if(isset($_SESSION['admin'])) {
        header("location: /project-php/backend");
    } else if (isset($_SESSION['customer'])) {
        header("location: /project-php/frontend/products");
    }
    header("location: /project-php/frontend/products");
?>